


                     eT 1.3 - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, T. Moitra, R. H. Myhre, A. C. Paul, S. Roet, M. Scavino, 
   A. Skeidsvoll, Å. H. Tveten
  ------------------------------------------------------------------------
   J. Chem. Phys. 152, 184103 (2020); https://doi.org/10.1063/5.0004713


   This is eT 1.3.0 Disco
  ------------------------------------------------------------
  Configuration date: 2021-06-07 12:48:33 UTC +02:00
  Git branch:         update-test-refs-new
  Git hash:           07052c810cfb89ed77f5f7b0b9d39be404f0338e
  Fortran compiler:   GNU 10.3.0
  C compiler:         GNU 10.3.0
  C++ compiler:       GNU 10.3.0
  LAPACK type:        MKL
  BLAS type:          MKL
  64-bit integers:    ON
  OpenMP:             ON
  PCM:                ON
  Forced batching:    OFF
  Runtime checks:     OFF
  ------------------------------------------------------------

  Calculation started: 2021-06-07 12:50:28 UTC +02:00


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: H2O He
     end system

     memory
       available: 8
     end memory

     do
       excited state
     end do

     method
        hf
        ccsd
     end method

     cc
       bath orbital
     end cc

     solver scf
        gradient threshold: 1.0d-11
        energy threshold: 1.0d-11
     end solver scf

     solver cholesky
        threshold: 1.0d-11
     end solver cholesky

     solver cc gs
        omega threshold: 1.0d-11
     end solver cc gs

     solver cc es
        ionization
        residual threshold: 1.0d-11
        singlet states: 2
        algorithm: diis
        left eigenvectors
     end solver cc es


  Running on 2 OMP threads
  Memory available for calculation: 8.000000 GB


  :: RHF wavefunction
  ======================

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  H     0.866810000000     0.601440000000     5.000000000000        1
        2  H    -0.866810000000     0.601440000000     5.000000000000        2
        3  O     0.000000000000    -0.075790000000     5.000000000000        3
        4 He     0.100000000000    -0.020000000000     7.530000000000        4
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  H     1.638033502034     1.136556880358     9.448630622825        1
        2  H    -1.638033502034     1.136556880358     9.448630622825        2
        3  O     0.000000000000    -0.143222342981     9.448630622825        3
        4 He     0.188972612457    -0.037794522491    14.229637717975        4
     ==============================================================================

  - Cholesky decomposition of AO overlap to get linearly independent AOs:

     Linear dependence threshold:             0.10E-05
     Number of atomic orbitals:               29
     Number of orthonormal atomic orbitals:   29

  - Molecular orbital details:

     Number of occupied orbitals:         6
     Number of virtual orbitals:         23
     Number of molecular orbitals:       29


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a RHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)


  1) Generate initial SAD density


  2) Calculation of reference state (SCF-DIIS algorithm)

  - SCF solver settings:

     Maximum iterations:                   100
     Acceleration type:                   diis

     Residual threshold:            0.1000E-10
     Energy threshold:              0.1000E-10

  - Setting initial AO density to sad

     Energy of initial guess:               -78.492022836321
     Number of electrons in guess:           12.000000000000

  - Screening and integral thresholds:

     Coulomb screening threshold:    0.1000E-16
     Exchange screening threshold:   0.1000E-14
     ERI cutoff:                     0.1000E-16
     One-electron integral  cutoff:  0.1000E-21
     Cumulative Fock threshold:      0.1000E+01

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (solver scf_e): memory
     Storage (solver scf_x): memory

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1           -78.796606592593     0.9053E-01     0.7880E+02
     2           -78.828675852657     0.7128E-01     0.3207E-01
     3           -78.843428154462     0.8290E-02     0.1475E-01
     4           -78.843809541675     0.2414E-02     0.3814E-03
     5           -78.843850846799     0.3301E-03     0.4131E-04
     6           -78.843851669326     0.5527E-04     0.8225E-06
     7           -78.843851692851     0.5865E-05     0.2353E-07
     8           -78.843851693557     0.2356E-05     0.7054E-09
     9           -78.843851693630     0.2071E-06     0.7380E-10
    10           -78.843851693631     0.1594E-07     0.4121E-12
    11           -78.843851693631     0.3322E-08     0.2842E-13
    12           -78.843851693631     0.1197E-08     0.2842E-13
    13           -78.843851693631     0.4264E-09     0.2842E-13
    14           -78.843851693631     0.1280E-09     0.2842E-13
    15           -78.843851693631     0.1479E-10     0.0000E+00
    16           -78.843851693631     0.2908E-11     0.4263E-13
  ---------------------------------------------------------------
  Convergence criterion met in 16 iterations!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.645629080248
     Nuclear repulsion energy:      12.116100574587
     Electronic energy:            -90.959952268218
     Total energy:                 -78.843851693631

  - Timings for the RHF ground state calculation

     Total wall time (sec):              0.30667
     Total cpu time (sec):               0.57533


  :: CCSD wavefunction
  =======================

     Bath orbital(s):         True

   - Number of orbitals:

     Occupied orbitals:    6
     Virtual orbitals:     24
     Molecular orbitals:   30
     Atomic orbitals:      29

   - Number of ground state amplitudes:

     Single excitation amplitudes:  144
     Double excitation amplitudes:  10440


  :: Excited state coupled cluster engine
  ==========================================

  Calculates the coupled cluster excitation vectors and excitation energies

  This is a CCSD excited state calculation.
  The following tasks will be performed:

     1) Cholesky decomposition of the electron repulsion integrals
     2) Preparation of MO basis and integrals
     3) Calculation of the ground state (diis algorithm)
     4) Calculation of the excited state (diis algorithm)


  1) Cholesky decomposition of the electron repulsion integrals

   - Cholesky decomposition of electronic repulsion integrals solver
  ---------------------------------------------------------------------

  Performs a Cholesky decomposition of the two-electron electronic repulsion 
  integrals in the atomic orbital basis,

  (ab|cd) = sum_J L_ab^J L_cd^J.

  Once the Cholesky basis has been determined, the vectors L^J are constructed 
  and stored to disk. These may either be used directly, or be transformed 
  to the MO basis for use in post-HF calculations. For more information, 
  see S. D. Folkestad, E. F. Kjønstad and H. Koch, JCP, 150(19), (2019)

  - Cholesky decomposition settings:

     Target threshold is:   0.10E-10
     Span factor:           0.10E-01
     Max qual:                  1000

  - Cholesky decomposition ao details:

     Total number of AOs:                    29
     Total number of shell pairs:           120
     Total number of AO pairs:              435

     Significant shell pairs:               117
     Significant AO pairs:                  430

     Construct shell pairs:                 120
     Construct AO pairs:                    435

  Iter.  #Sign. ao pairs / shell pairs   Max diagonal    #Qualified    #Cholesky    Cholesky array size
  -------------------------------------------------------------------------------------------------------
     1               405 /     107       0.47383E+01         147             42             17010
     2               318 /      92       0.47165E-01         234            111             35298
     3               246 /      74       0.46944E-03         178            183             45018
     4               173 /      51       0.38270E-05         145            265             45845
     5                70 /      18       0.38106E-07          78            324             22680
     6                 0 /       0       0.37202E-09          33            345                 0
  -------------------------------------------------------------------------------------------------------

  - Summary of Cholesky decomposition of electronic repulsion integrals:

     Final number of Cholesky vectors: 345

 - Testing the Cholesky decomposition decomposition electronic repulsion integrals:

     Maximal difference between approximate and actual diagonal:              0.6608E-11
     Minimal element of difference between approximate and actual diagonal:  -0.1238E-14

  - Settings for integral handling:

     Cholesky vectors in memory: True
     ERI matrix in memory:       False
     T1 ERI matrix in memory:    True

  - Finished decomposing the ERIs.

     Total wall time (sec):              0.11258
     Total cpu time (sec):               0.22456


  2) Preparation of MO basis and integrals


  3) Calculation of the ground state (diis algorithm)

   - DIIS coupled cluster ground state solver
  ----------------------------------------------

  A DIIS CC ground state amplitude equations solver. It uses an extrapolation 
  of previous quasi-Newton perturbation theory estimates of the next amplitudes. 
  See Helgaker et al., Molecular Electronic Structure Theory, Chapter 
  13.

  - CC ground state solver settings:

     Residual threshold:            0.1000E-10
     Max number of iterations:      100

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (cc_gs_diis_e): file
     Storage (cc_gs_diis_x): file

  Iteration    Energy (a.u.)        |omega|       Delta E (a.u.)
  ---------------------------------------------------------------
    1           -79.084241931608     0.9391E-01     0.7908E+02
    2           -79.092586666607     0.2720E-01     0.8345E-02
    3           -79.099408028306     0.7507E-02     0.6821E-02
    4           -79.100345871309     0.2095E-02     0.9378E-03
    5           -79.100371860301     0.5154E-03     0.2599E-04
    6           -79.100393801016     0.2313E-03     0.2194E-04
    7           -79.100385611260     0.4933E-04     0.8190E-05
    8           -79.100384217523     0.1180E-04     0.1394E-05
    9           -79.100383621794     0.4135E-05     0.5957E-06
   10           -79.100383427012     0.1779E-05     0.1948E-06
   11           -79.100383466393     0.6705E-06     0.3938E-07
   12           -79.100383474629     0.2989E-06     0.8237E-08
   13           -79.100383487351     0.1028E-06     0.1272E-07
   14           -79.100383481864     0.3056E-07     0.5487E-08
   15           -79.100383481091     0.6508E-08     0.7730E-09
   16           -79.100383481302     0.2112E-08     0.2109E-09
   17           -79.100383481484     0.6588E-09     0.1819E-09
   18           -79.100383481546     0.1594E-09     0.6158E-10
   19           -79.100383481554     0.5326E-10     0.7901E-11
   20           -79.100383481555     0.2008E-10     0.1208E-11
   21           -79.100383481555     0.7516E-11     0.6679E-12
  ---------------------------------------------------------------
  Convergence criterion met in 21 iterations!

  - Ground state summary:

     Final ground state energy (a.u.):   -79.100383481555

     Correlation energy (a.u.):           -0.256531787925

     Largest single amplitudes:
     -----------------------------------
        a       i         t(a,i)
     -----------------------------------
        1      5        0.014740597524
       14      4       -0.009546856220
        7      4        0.008284826482
       15      5       -0.006124828874
        4      5        0.005606072696
        6      2        0.005476844297
        2      4        0.005318591700
       13      5        0.005269818336
        5      6        0.004933006906
       11      6       -0.003454309399
     ------------------------------------

     Largest double amplitudes:
     --------------------------------------------------
        a      i       b      j         t(ai,bj)
     --------------------------------------------------
        2      4       2      4       -0.047351708919
        5      6       5      6       -0.046240574404
        9      3       9      3       -0.041367012248
        3      4       3      4       -0.036659067517
        6      5       6      5       -0.034554012170
        1      5       1      5       -0.034177347751
       16      3      16      3       -0.032108235347
       17      3      17      3       -0.032052553603
       18      3      18      3       -0.031351828684
        2      4       3      4       -0.029701270698
     --------------------------------------------------

  - Finished solving the CCSD ground state equations

     Total wall time (sec):              0.17659
     Total cpu time (sec):               0.35253


  4) Calculation of the excited state (diis algorithm)
     Calculating left vectors

   - DIIS coupled cluster excited state solver
  -----------------------------------------------

  A DIIS solver that solves for the lowest eigenvalues and  the right 
  eigenvectors of the Jacobian matrix, A. The eigenvalue  problem is solved 
  by DIIS extrapolation of residuals for each  eigenvector until the convergence 
  criteria are met.

  More on the DIIS algorithm can be found in P. Pulay, Chemical Physics 
  Letters, 73(2), 393-398 (1980).

  - Settings for coupled cluster excited state solver (DIIS):

     Calculation type:    ionize
     Excitation vectors:  left

     Residual threshold:            0.1000E-10

     Number of singlet states:               2
     Max number of iterations:             100

  - DIIS tool settings:

     DIIS dimension:  20

     Storage (diis_cc_es_001_e): file
     Storage (diis_cc_es_001_x): file

  - DIIS tool settings:

     DIIS dimension:  20

     Storage (diis_cc_es_002_e): file
     Storage (diis_cc_es_002_x): file

  Iteration:                  1

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.542905056405       0.7743E+00
     2      0.605921789240       0.6938E+00
  -----------------------------------------------

  Iteration:                  2

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.472062274189       0.1710E+00
     2      0.540656173708       0.1466E+00
  -----------------------------------------------

  Iteration:                  3

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.449586335819       0.1331E+00
     2      0.539312102536       0.1438E+00
  -----------------------------------------------

  Iteration:                  4

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.418988209628       0.3057E-01
     2      0.503373759778       0.3325E-01
  -----------------------------------------------

  Iteration:                  5

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.415140826833       0.1502E-01
     2      0.500676869864       0.2436E-01
  -----------------------------------------------

  Iteration:                  6

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.413499416471       0.5134E-02
     2      0.500853915874       0.1668E-01
  -----------------------------------------------

  Iteration:                  7

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.412344545459       0.7695E-03
     2      0.499712475645       0.8831E-02
  -----------------------------------------------

  Iteration:                  8

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.412241343743       0.2190E-03
     2      0.498650543059       0.3627E-02
  -----------------------------------------------

  Iteration:                  9

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.412266696569       0.1022E-03
     2      0.498559903120       0.7201E-03
  -----------------------------------------------

  Iteration:                 10

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.412276587595       0.4372E-04
     2      0.498640898816       0.1656E-03
  -----------------------------------------------

  Iteration:                 11

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.412273008723       0.1653E-04
     2      0.498645552693       0.6813E-04
  -----------------------------------------------

  Iteration:                 12

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.412270925988       0.3458E-05
     2      0.498641764236       0.2751E-04
  -----------------------------------------------

  Iteration:                 13

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.412270756427       0.7767E-06
     2      0.498638279391       0.5565E-05
  -----------------------------------------------

  Iteration:                 14

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.412270827006       0.1388E-06
     2      0.498637534080       0.9568E-06
  -----------------------------------------------

  Iteration:                 15

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.412270841344       0.2931E-07
     2      0.498637465606       0.2513E-06
  -----------------------------------------------

  Iteration:                 16

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.412270841087       0.4235E-08
     2      0.498637442856       0.4467E-07
  -----------------------------------------------

  Iteration:                 17

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.412270841102       0.1446E-08
     2      0.498637437253       0.7974E-08
  -----------------------------------------------

  Iteration:                 18

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.412270841190       0.2915E-09
     2      0.498637437060       0.1973E-08
  -----------------------------------------------

  Iteration:                 19

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.412270841209       0.6023E-10
     2      0.498637437191       0.5293E-09
  -----------------------------------------------

  Iteration:                 20

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.412270841206       0.1355E-10
     2      0.498637437211       0.1497E-09
  -----------------------------------------------

  Iteration:                 21

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.412270841205       0.3032E-11
     2      0.498637437211       0.3798E-10
  -----------------------------------------------

  Iteration:                 22

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.412270841205       0.3032E-11
     2      0.498637437212       0.7844E-11
  -----------------------------------------------
  Convergence criterion met in 22 iterations!

  - Resorting roots according to excitation energy.

  - Stored converged states to file.

  - Excitation vector amplitudes:

     Electronic state nr. 1

     Energy (Hartree):                  0.412270841205
     Fraction singles (|L1|/|L|):       0.910517408911

     Largest single amplitudes:
     -----------------------------------
        a       i         L(a,i)
     -----------------------------------
       24      6        0.910507479800
       24      5        0.003267854286
       24      3       -0.002682947109
       24      2        0.000440047092
       24      4        0.000102172348
       24      1       -0.000001223650
        6      1        0.000000000000
        7      1        0.000000000000
        8      1        0.000000000000
        9      1        0.000000000000
     ------------------------------------

     Largest double amplitudes:
     --------------------------------------------------
        a      i       b      j         L(ai,bj)
     --------------------------------------------------
        2      4      24      6       -0.222623771438
        1      5      24      6       -0.164492886732
        6      5      24      6        0.128612320900
        7      4      24      6        0.109234945776
        5      6      24      6       -0.096728718404
        1      2      24      6       -0.096467416391
        4      5      24      6       -0.086572252340
       24      4       2      6        0.082595727979
        3      4      24      6       -0.079842539098
       24      5       1      6        0.068827233051
     --------------------------------------------------

     Electronic state nr. 2

     Energy (Hartree):                  0.498637437212
     Fraction singles (|L1|/|L|):       0.928418165681

     Largest single amplitudes:
     -----------------------------------
        a       i         L(a,i)
     -----------------------------------
       24      5        0.927290919138
       24      2       -0.045558837115
       24      6       -0.003541885898
       24      3       -0.001902812695
       24      1        0.000261422156
       24      4        0.000004226855
        6      1        0.000000000000
        7      1        0.000000000000
        8      1        0.000000000000
        9      1        0.000000000000
     ------------------------------------

     Largest double amplitudes:
     --------------------------------------------------
        a      i       b      j         L(ai,bj)
     --------------------------------------------------
        2      4      24      5       -0.211908718022
       24      5       5      6       -0.133987055172
       24      4       2      5        0.093918679859
        7      4      24      5        0.092987797085
        1      5      24      5       -0.092016654604
        1      2      24      5       -0.088396254042
        6      5      24      5        0.078208904343
        3      4      24      5       -0.071663426175
       24      2       1      5        0.052058576372
        5      5      24      6        0.051821221067
     --------------------------------------------------

     - Electronic excitation energies:

                                      Excitation energy
                          ------------------------------------------
      State                (Hartree)             (eV)
     ---------------------------------------------------------------
        1                  0.412270841205       11.218461004812
        2                  0.498637437212       13.568615787989
     ---------------------------------------------------------------
     eV/Hartree (CODATA 2014): 27.21138602

  - Finished solving the CCSD excited state equations (left)

     Total wall time (sec):              0.19200
     Total cpu time (sec):               0.38396

  - Timings for the CCSD excited state calculation

     Total wall time (sec):              0.48154
     Total cpu time (sec):               0.96143

  ------------------------------------------------------------

  Peak memory usage during the execution of eT: 17.533784 MB

  Total wall time in eT (sec):              0.80823
  Total cpu time in eT (sec):               1.56650

  Calculation ended: 2021-06-07 12:50:28 UTC +02:00

  - Implementation references:

     eT: https://doi.org/10.1063/5.0004713
     Cholesky decomposition of ERIs: https://doi.org/10.1063/1.5083802

  eT terminated successfully!




                     eT 1.3 - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, T. Moitra, R. H. Myhre, A. C. Paul, S. Roet, M. Scavino, 
   A. Skeidsvoll, Å. H. Tveten
  ------------------------------------------------------------------------
   J. Chem. Phys. 152, 184103 (2020); https://doi.org/10.1063/5.0004713


   This is eT 1.3.0 Disco
  ------------------------------------------------------------
  Configuration date: 2021-06-07 12:48:33 UTC +02:00
  Git branch:         update-test-refs-new
  Git hash:           07052c810cfb89ed77f5f7b0b9d39be404f0338e
  Fortran compiler:   GNU 10.3.0
  C compiler:         GNU 10.3.0
  C++ compiler:       GNU 10.3.0
  LAPACK type:        MKL
  BLAS type:          MKL
  64-bit integers:    ON
  OpenMP:             ON
  PCM:                ON
  Forced batching:    OFF
  Runtime checks:     OFF
  ------------------------------------------------------------

  Calculation started: 2021-06-07 12:50:08 UTC +02:00


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: H2O He
        charge: 0
     end system

     do
       excited state
     end do

     memory
        available: 8
     end memory

     solver cholesky
        threshold: 1.0d-12
     end solver cholesky

     solver scf
        energy threshold:   1.0d-11
        gradient threshold: 1.0d-11
     end solver scf

     method
        hf
        lowmem-cc2
     end method

     solver cc gs
        omega threshold:  1.0d-11
        energy threshold: 1.0d-11
     end solver cc gs

     solver cc es
        algorithm:          diis
        singlet states:     3
        residual threshold: 1.0d-11
        energy threshold:   1.0d-11
     end solver cc es


  Running on 2 OMP threads
  Memory available for calculation: 8.000000 GB


  :: RHF wavefunction
  ======================

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  H     0.866810000000     0.601440000000     5.000000000000        1
        2  H    -0.866810000000     0.601440000000     5.000000000000        2
        3  O     0.000000000000    -0.075790000000     5.000000000000        3
        4 He     0.100000000000    -0.020000000000     7.530000000000        4
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  H     1.638033502034     1.136556880358     9.448630622825        1
        2  H    -1.638033502034     1.136556880358     9.448630622825        2
        3  O     0.000000000000    -0.143222342981     9.448630622825        3
        4 He     0.188972612457    -0.037794522491    14.229637717975        4
     ==============================================================================

  - Cholesky decomposition of AO overlap to get linearly independent AOs:

     Linear dependence threshold:             0.10E-05
     Number of atomic orbitals:               29
     Number of orthonormal atomic orbitals:   29

  - Molecular orbital details:

     Number of occupied orbitals:         6
     Number of virtual orbitals:         23
     Number of molecular orbitals:       29


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a RHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)


  1) Generate initial SAD density


  2) Calculation of reference state (SCF-DIIS algorithm)

  - SCF solver settings:

     Maximum iterations:                   100
     Acceleration type:                   diis

     Residual threshold:            0.1000E-10
     Energy threshold:              0.1000E-10

  - Setting initial AO density to sad

     Energy of initial guess:               -78.492022836321
     Number of electrons in guess:           12.000000000000

  - Screening and integral thresholds:

     Coulomb screening threshold:    0.1000E-16
     Exchange screening threshold:   0.1000E-14
     ERI cutoff:                     0.1000E-16
     One-electron integral  cutoff:  0.1000E-21
     Cumulative Fock threshold:      0.1000E+01

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (solver scf_e): memory
     Storage (solver scf_x): memory

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1           -78.796606592593     0.9053E-01     0.7880E+02
     2           -78.828675852657     0.7128E-01     0.3207E-01
     3           -78.843428154462     0.8290E-02     0.1475E-01
     4           -78.843809541675     0.2414E-02     0.3814E-03
     5           -78.843850846799     0.3301E-03     0.4131E-04
     6           -78.843851669326     0.5527E-04     0.8225E-06
     7           -78.843851692851     0.5865E-05     0.2353E-07
     8           -78.843851693557     0.2356E-05     0.7054E-09
     9           -78.843851693630     0.2071E-06     0.7371E-10
    10           -78.843851693631     0.1594E-07     0.3979E-12
    11           -78.843851693631     0.3322E-08     0.1421E-13
    12           -78.843851693631     0.1197E-08     0.1421E-13
    13           -78.843851693631     0.4264E-09     0.1421E-13
    14           -78.843851693631     0.1280E-09     0.1421E-13
    15           -78.843851693631     0.1479E-10     0.1421E-13
    16           -78.843851693631     0.2908E-11     0.2842E-13
  ---------------------------------------------------------------
  Convergence criterion met in 16 iterations!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.645629080248
     Nuclear repulsion energy:      12.116100574587
     Electronic energy:            -90.959952268218
     Total energy:                 -78.843851693631

  - Timings for the RHF ground state calculation

     Total wall time (sec):              0.32036
     Total cpu time (sec):               0.56946


  :: LOW MEMORY CC2 wavefunction
  =================================

     Bath orbital(s):         False

   - Number of orbitals:

     Occupied orbitals:    6
     Virtual orbitals:     23
     Molecular orbitals:   29
     Atomic orbitals:      29

   - Number of ground state amplitudes:

     Single excitation amplitudes:  138


  :: Excited state coupled cluster engine
  ==========================================

  Calculates the coupled cluster excitation vectors and excitation energies

  This is a LOW MEMORY CC2 excited state calculation.
  The following tasks will be performed:

     1) Cholesky decomposition of the electron repulsion integrals
     2) Preparation of MO basis and integrals
     3) Calculation of the ground state (diis algorithm)
     4) Calculation of the excited state (diis algorithm)


  1) Cholesky decomposition of the electron repulsion integrals

   - Cholesky decomposition of electronic repulsion integrals solver
  ---------------------------------------------------------------------

  Performs a Cholesky decomposition of the two-electron electronic repulsion 
  integrals in the atomic orbital basis,

  (ab|cd) = sum_J L_ab^J L_cd^J.

  Once the Cholesky basis has been determined, the vectors L^J are constructed 
  and stored to disk. These may either be used directly, or be transformed 
  to the MO basis for use in post-HF calculations. For more information, 
  see S. D. Folkestad, E. F. Kjønstad and H. Koch, JCP, 150(19), (2019)

  - Cholesky decomposition settings:

     Target threshold is:   0.10E-11
     Span factor:           0.10E-01
     Max qual:                  1000

  - Cholesky decomposition ao details:

     Total number of AOs:                    29
     Total number of shell pairs:           120
     Total number of AO pairs:              435

     Significant shell pairs:               118
     Significant AO pairs:                  431

     Construct shell pairs:                 120
     Construct AO pairs:                    435

  Iter.  #Sign. ao pairs / shell pairs   Max diagonal    #Qualified    #Cholesky    Cholesky array size
  -------------------------------------------------------------------------------------------------------
     1               406 /     108       0.47383E+01         147             42             17052
     2               319 /      93       0.47165E-01         234            111             35409
     3               250 /      76       0.46944E-03         178            183             45750
     4               187 /      55       0.38270E-05         145            265             49555
     5                87 /      25       0.38106E-07          78            324             28188
     6                21 /       3       0.37202E-09          43            349              7329
     7                 0 /       0       0.36652E-11           7            354                 0
  -------------------------------------------------------------------------------------------------------

  - Summary of Cholesky decomposition of electronic repulsion integrals:

     Final number of Cholesky vectors: 354

 - Testing the Cholesky decomposition decomposition electronic repulsion integrals:

     Maximal difference between approximate and actual diagonal:              0.7161E-12
     Minimal element of difference between approximate and actual diagonal:  -0.1238E-14

  - Settings for integral handling:

     Cholesky vectors in memory: True
     ERI matrix in memory:       False
     T1 ERI matrix in memory:    False

  - Finished decomposing the ERIs.

     Total wall time (sec):              0.11367
     Total cpu time (sec):               0.22565


  2) Preparation of MO basis and integrals


  3) Calculation of the ground state (diis algorithm)

   - DIIS coupled cluster ground state solver
  ----------------------------------------------

  A DIIS CC ground state amplitude equations solver. It uses an extrapolation 
  of previous quasi-Newton perturbation theory estimates of the next amplitudes. 
  See Helgaker et al., Molecular Electronic Structure Theory, Chapter 
  13.

  - CC ground state solver settings:

     Residual threshold:            0.1000E-10
     Energy threshold:              0.1000E-10
     Max number of iterations:      100

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (cc_gs_diis_e): file
     Storage (cc_gs_diis_x): file

  Iteration    Energy (a.u.)        |omega|       Delta E (a.u.)
  ---------------------------------------------------------------
    1           -79.084241931608     0.3734E-01     0.7908E+02
    2           -79.085431671929     0.7567E-02     0.1190E-02
    3           -79.085759807021     0.9940E-03     0.3281E-03
    4           -79.085768585650     0.1833E-03     0.8779E-05
    5           -79.085769937826     0.3615E-04     0.1352E-05
    6           -79.085769725742     0.6004E-05     0.2121E-06
    7           -79.085769729164     0.2063E-05     0.3422E-08
    8           -79.085769729229     0.3646E-06     0.6527E-10
    9           -79.085769729230     0.3393E-07     0.9095E-12
   10           -79.085769729553     0.4633E-08     0.3225E-09
   11           -79.085769729585     0.9303E-09     0.3266E-10
   12           -79.085769729575     0.2416E-09     0.1074E-10
   13           -79.085769729575     0.4926E-10     0.4690E-12
   14           -79.085769729575     0.6369E-11     0.3837E-12
  ---------------------------------------------------------------
  Convergence criterion met in 14 iterations!

  - Ground state summary:

     Final ground state energy (a.u.):   -79.085769729575

     Correlation energy (a.u.):           -0.241918035945

     Largest single amplitudes:
     -----------------------------------
        a       i         t(a,i)
     -----------------------------------
        1      5        0.015071367767
       14      4       -0.009516336778
        7      4        0.008547796583
       15      5       -0.006232215147
        5      6        0.005875107063
        6      2        0.005220429230
       13      5        0.005212699581
        2      4        0.005071084622
       11      6       -0.003616248565
        4      5        0.003233309228
     ------------------------------------

  - Finished solving the LOW MEMORY CC2 ground state equations

     Total wall time (sec):              0.04121
     Total cpu time (sec):               0.08510


  4) Calculation of the excited state (diis algorithm)
     Calculating right vectors

   - DIIS coupled cluster excited state solver
  -----------------------------------------------

  A DIIS solver that solves for the lowest eigenvalues and  the right 
  eigenvectors of the Jacobian matrix, A. The eigenvalue  problem is solved 
  by DIIS extrapolation of residuals for each  eigenvector until the convergence 
  criteria are met.

  More on the DIIS algorithm can be found in P. Pulay, Chemical Physics 
  Letters, 73(2), 393-398 (1980).

  - Settings for coupled cluster excited state solver (DIIS):

     Calculation type:    valence
     Excitation vectors:  right

     Residual threshold:            0.1000E-10
     Energy threshold:              0.1000E-10

     Number of singlet states:               3
     Max number of iterations:             100

  - DIIS tool settings:

     DIIS dimension:  20

     Storage (diis_cc_es_001_e): file
     Storage (diis_cc_es_001_x): file

  - DIIS tool settings:

     DIIS dimension:  20

     Storage (diis_cc_es_002_e): file
     Storage (diis_cc_es_002_x): file

  - DIIS tool settings:

     DIIS dimension:  20

     Storage (diis_cc_es_003_e): file
     Storage (diis_cc_es_003_x): file

  Iteration:                  1

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.268022491098       0.9683E-01
     2      0.389863521848       0.1431E+00
     3      0.343849924367       0.1043E+00
  -----------------------------------------------

  Iteration:                  2

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.246001145685       0.3074E-01
     2      0.354550155999       0.3597E-01
     3      0.313696610076       0.4088E-01
  -----------------------------------------------

  Iteration:                  3

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245997351305       0.5132E-02
     2      0.354575195080       0.1336E-01
     3      0.312506779505       0.7382E-02
  -----------------------------------------------

  Iteration:                  4

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245986218473       0.7892E-03
     2      0.354160692791       0.4589E-02
     3      0.312505152797       0.1822E-02
  -----------------------------------------------

  Iteration:                  5

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245983313179       0.2042E-03
     2      0.354136122184       0.4697E-02
     3      0.312490794572       0.3198E-03
  -----------------------------------------------

  Iteration:                  6

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245983693543       0.1221E-03
     2      0.354269353687       0.2336E-02
     3      0.312493435867       0.1926E-03
  -----------------------------------------------

  Iteration:                  7

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245983318085       0.3089E-04
     2      0.354258090425       0.5440E-03
     3      0.312494628547       0.2477E-03
  -----------------------------------------------

  Iteration:                  8

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245983434537       0.7364E-05
     2      0.354257087211       0.7312E-04
     3      0.312489579336       0.5173E-03
  -----------------------------------------------

  Iteration:                  9

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245983447255       0.4095E-05
     2      0.354257049664       0.5996E-04
     3      0.312493050657       0.4190E-04
  -----------------------------------------------

  Iteration:                 10

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245983430456       0.2852E-05
     2      0.354257109589       0.5197E-04
     3      0.312493293691       0.2127E-04
  -----------------------------------------------

  Iteration:                 11

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245983445688       0.6671E-06
     2      0.354257096221       0.1707E-04
     3      0.312493038260       0.1314E-04
  -----------------------------------------------

  Iteration:                 12

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245983440977       0.7988E-07
     2      0.354257048873       0.1187E-04
     3      0.312493153715       0.3128E-05
  -----------------------------------------------

  Iteration:                 13

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245983441300       0.2642E-07
     2      0.354257070476       0.4267E-05
     3      0.312493185914       0.6300E-05
  -----------------------------------------------

  Iteration:                 14

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245983441336       0.4185E-08
     2      0.354257073627       0.2066E-05
     3      0.312493137695       0.2513E-06
  -----------------------------------------------

  Iteration:                 15

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245983441341       0.1093E-08
     2      0.354257074440       0.8731E-06
     3      0.312493141027       0.6519E-07
  -----------------------------------------------

  Iteration:                 16

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245983441338       0.2555E-09
     2      0.354257073334       0.1716E-06
     3      0.312493140680       0.1880E-07
  -----------------------------------------------

  Iteration:                 17

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245983441338       0.9825E-10
     2      0.354257072900       0.2673E-07
     3      0.312493140864       0.4443E-08
  -----------------------------------------------

  Iteration:                 18

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245983441338       0.2002E-10
     2      0.354257072906       0.5984E-08
     3      0.312493140812       0.1265E-08
  -----------------------------------------------

  Iteration:                 19

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245983441338       0.1814E-11
     2      0.354257072913       0.9035E-09
     3      0.312493140828       0.4576E-09
  -----------------------------------------------

  Iteration:                 20

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245983441338       0.1814E-11
     2      0.354257072917       0.1016E-09
     3      0.312493140823       0.7886E-10
  -----------------------------------------------

  Iteration:                 21

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245983441338       0.1814E-11
     2      0.354257072916       0.1861E-10
     3      0.312493140824       0.2861E-11
  -----------------------------------------------

  Iteration:                 22

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245983441338       0.1814E-11
     2      0.354257072916       0.4675E-11
     3      0.312493140824       0.2861E-11
  -----------------------------------------------
  Convergence criterion met in 22 iterations!

  - Resorting roots according to excitation energy.

  - Stored converged states to file.

  - Excitation vector amplitudes:

     Electronic state nr. 1

     Energy (Hartree):                  0.245983441338
     Fraction singles (|R1|/|R|):       1.000000000000

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        1      6        0.992570977340
        4      6        0.108934949543
        6      6       -0.036841280984
       13      6       -0.030571764350
        1      3       -0.011873170889
       22      6        0.008128864923
        1      5        0.008036840172
       19      5       -0.007869044457
        9      6       -0.007462543552
       20      4       -0.007116746218
     ------------------------------------

     Electronic state nr. 2

     Energy (Hartree):                  0.354257072916
     Fraction singles (|R1|/|R|):       1.000000000000

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        2      6        0.983888959785
        3      6        0.161510332238
        7      6       -0.071460933884
       14      6        0.021752345804
        2      3       -0.011826768100
       12      6       -0.006327958190
        2      5        0.005484089348
        8      2       -0.005408908490
        5      4       -0.004596274767
       11      4       -0.003334068739
     ------------------------------------

     Electronic state nr. 3

     Energy (Hartree):                  0.312493140824
     Fraction singles (|R1|/|R|):       1.000000000000

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        1      5        0.981722161081
        2      4       -0.134704861788
        4      5        0.078127172213
        1      2       -0.069420463820
        3      4       -0.052712335430
        5      6       -0.040555034655
       13      5       -0.029288514830
        7      4        0.026531873269
        4      2       -0.018792517154
       10      5        0.013280743592
     ------------------------------------

     - Electronic excitation energies:

                                      Excitation energy
                          ------------------------------------------
      State                (Hartree)             (eV)
     ---------------------------------------------------------------
        1                  0.245983441338        6.693550376783
        2                  0.312493140824        8.503371483564
        3                  0.354257072916        9.639825961443
     ---------------------------------------------------------------
     eV/Hartree (CODATA 2014): 27.21138602

  - Finished solving the LOW MEMORY CC2 excited state equations (right)

     Total wall time (sec):              0.35144
     Total cpu time (sec):               0.70165

  - Timings for the LOW MEMORY CC2 excited state calculation

     Total wall time (sec):              0.50692
     Total cpu time (sec):               1.01301

  ------------------------------------------------------------

  Peak memory usage during the execution of eT: 10.695640 MB

  Total wall time in eT (sec):              0.84707
  Total cpu time in eT (sec):               1.61016

  Calculation ended: 2021-06-07 12:50:09 UTC +02:00

  - Implementation references:

     eT: https://doi.org/10.1063/5.0004713
     Cholesky decomposition of ERIs: https://doi.org/10.1063/1.5083802

  eT terminated successfully!

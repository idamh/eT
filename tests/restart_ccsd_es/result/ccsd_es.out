


                     eT 1.3 - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, T. Moitra, R. H. Myhre, A. C. Paul, S. Roet, M. Scavino, 
   A. Skeidsvoll, Å. H. Tveten
  ------------------------------------------------------------------------
   J. Chem. Phys. 152, 184103 (2020); https://doi.org/10.1063/5.0004713


   This is eT 1.3.0 Disco
  ------------------------------------------------------------
  Configuration date: 2021-06-07 12:48:33 UTC +02:00
  Git branch:         update-test-refs-new
  Git hash:           07052c810cfb89ed77f5f7b0b9d39be404f0338e
  Fortran compiler:   GNU 10.3.0
  C compiler:         GNU 10.3.0
  C++ compiler:       GNU 10.3.0
  LAPACK type:        MKL
  BLAS type:          MKL
  64-bit integers:    ON
  OpenMP:             ON
  PCM:                ON
  Forced batching:    OFF
  Runtime checks:     OFF
  ------------------------------------------------------------

  Calculation started: 2021-06-07 12:50:54 UTC +02:00


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: H2O He
        charge: 0
     end system

     do
       excited state
     end do

     memory
        available: 8
     end memory

     solver cholesky
        threshold: 1.0d-12
     end solver cholesky

     solver scf
        energy threshold:   1.0d-11
        gradient threshold: 1.0d-11
     end solver scf

     method
        hf
        ccsd
     end method

     solver cc gs
        omega threshold:  1.0d-11
        energy threshold: 1.0d-11
     end solver cc gs

     solver cc es
        algorithm:          davidson
        singlet states:     3
        residual threshold: 1.0d-11
        energy threshold:   1.0d-11
     end solver cc es


  Running on 2 OMP threads
  Memory available for calculation: 8.000000 GB


  :: RHF wavefunction
  ======================

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  H     0.866810000000     0.601440000000     5.000000000000        1
        2  H    -0.866810000000     0.601440000000     5.000000000000        2
        3  O     0.000000000000    -0.075790000000     5.000000000000        3
        4 He     0.100000000000    -0.020000000000     7.530000000000        4
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  H     1.638033502034     1.136556880358     9.448630622825        1
        2  H    -1.638033502034     1.136556880358     9.448630622825        2
        3  O     0.000000000000    -0.143222342981     9.448630622825        3
        4 He     0.188972612457    -0.037794522491    14.229637717975        4
     ==============================================================================

  - Cholesky decomposition of AO overlap to get linearly independent AOs:

     Linear dependence threshold:             0.10E-05
     Number of atomic orbitals:               29
     Number of orthonormal atomic orbitals:   29

  - Molecular orbital details:

     Number of occupied orbitals:         6
     Number of virtual orbitals:         23
     Number of molecular orbitals:       29


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a RHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)


  1) Generate initial SAD density


  2) Calculation of reference state (SCF-DIIS algorithm)

  - SCF solver settings:

     Maximum iterations:                   100
     Acceleration type:                   diis

     Residual threshold:            0.1000E-10
     Energy threshold:              0.1000E-10

  - Setting initial AO density to sad

     Energy of initial guess:               -78.492022836321
     Number of electrons in guess:           12.000000000000

  - Screening and integral thresholds:

     Coulomb screening threshold:    0.1000E-16
     Exchange screening threshold:   0.1000E-14
     ERI cutoff:                     0.1000E-16
     One-electron integral  cutoff:  0.1000E-21
     Cumulative Fock threshold:      0.1000E+01

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (solver scf_e): memory
     Storage (solver scf_x): memory

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1           -78.796606592593     0.9053E-01     0.7880E+02
     2           -78.828675852657     0.7128E-01     0.3207E-01
     3           -78.843428154462     0.8290E-02     0.1475E-01
     4           -78.843809541675     0.2414E-02     0.3814E-03
     5           -78.843850846799     0.3301E-03     0.4131E-04
     6           -78.843851669326     0.5527E-04     0.8225E-06
     7           -78.843851692851     0.5865E-05     0.2353E-07
     8           -78.843851693557     0.2356E-05     0.7054E-09
     9           -78.843851693630     0.2071E-06     0.7374E-10
    10           -78.843851693631     0.1594E-07     0.4263E-12
    11           -78.843851693631     0.3322E-08     0.4263E-13
    12           -78.843851693631     0.1197E-08     0.1421E-13
    13           -78.843851693631     0.4264E-09     0.0000E+00
    14           -78.843851693631     0.1280E-09     0.2842E-13
    15           -78.843851693631     0.1479E-10     0.2842E-13
    16           -78.843851693631     0.2906E-11     0.1421E-13
  ---------------------------------------------------------------
  Convergence criterion met in 16 iterations!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.645629080248
     Nuclear repulsion energy:      12.116100574587
     Electronic energy:            -90.959952268218
     Total energy:                 -78.843851693631

  - Timings for the RHF ground state calculation

     Total wall time (sec):              0.32682
     Total cpu time (sec):               0.57580


  :: CCSD wavefunction
  =======================

     Bath orbital(s):         False

   - Number of orbitals:

     Occupied orbitals:    6
     Virtual orbitals:     23
     Molecular orbitals:   29
     Atomic orbitals:      29

   - Number of ground state amplitudes:

     Single excitation amplitudes:  138
     Double excitation amplitudes:  9591


  :: Excited state coupled cluster engine
  ==========================================

  Calculates the coupled cluster excitation vectors and excitation energies

  This is a CCSD excited state calculation.
  The following tasks will be performed:

     1) Cholesky decomposition of the electron repulsion integrals
     2) Preparation of MO basis and integrals
     3) Calculation of the ground state (diis algorithm)
     4) Calculation of the excited state (davidson algorithm)


  1) Cholesky decomposition of the electron repulsion integrals

   - Cholesky decomposition of electronic repulsion integrals solver
  ---------------------------------------------------------------------

  Performs a Cholesky decomposition of the two-electron electronic repulsion 
  integrals in the atomic orbital basis,

  (ab|cd) = sum_J L_ab^J L_cd^J.

  Once the Cholesky basis has been determined, the vectors L^J are constructed 
  and stored to disk. These may either be used directly, or be transformed 
  to the MO basis for use in post-HF calculations. For more information, 
  see S. D. Folkestad, E. F. Kjønstad and H. Koch, JCP, 150(19), (2019)

  - Cholesky decomposition settings:

     Target threshold is:   0.10E-11
     Span factor:           0.10E-01
     Max qual:                  1000

  - Cholesky decomposition ao details:

     Total number of AOs:                    29
     Total number of shell pairs:           120
     Total number of AO pairs:              435

     Significant shell pairs:               118
     Significant AO pairs:                  431

     Construct shell pairs:                 120
     Construct AO pairs:                    435

  Iter.  #Sign. ao pairs / shell pairs   Max diagonal    #Qualified    #Cholesky    Cholesky array size
  -------------------------------------------------------------------------------------------------------
     1               406 /     108       0.47383E+01         147             42             17052
     2               319 /      93       0.47165E-01         234            111             35409
     3               250 /      76       0.46944E-03         178            183             45750
     4               187 /      55       0.38270E-05         145            265             49555
     5                87 /      25       0.38106E-07          78            324             28188
     6                21 /       3       0.37202E-09          43            349              7329
     7                 0 /       0       0.36652E-11           7            354                 0
  -------------------------------------------------------------------------------------------------------

  - Summary of Cholesky decomposition of electronic repulsion integrals:

     Final number of Cholesky vectors: 354

 - Testing the Cholesky decomposition decomposition electronic repulsion integrals:

     Maximal difference between approximate and actual diagonal:              0.7161E-12
     Minimal element of difference between approximate and actual diagonal:  -0.1238E-14

  - Settings for integral handling:

     Cholesky vectors in memory: True
     ERI matrix in memory:       False
     T1 ERI matrix in memory:    True

  - Finished decomposing the ERIs.

     Total wall time (sec):              0.11954
     Total cpu time (sec):               0.24025


  2) Preparation of MO basis and integrals


  3) Calculation of the ground state (diis algorithm)

   - DIIS coupled cluster ground state solver
  ----------------------------------------------

  A DIIS CC ground state amplitude equations solver. It uses an extrapolation 
  of previous quasi-Newton perturbation theory estimates of the next amplitudes. 
  See Helgaker et al., Molecular Electronic Structure Theory, Chapter 
  13.

  - CC ground state solver settings:

     Residual threshold:            0.1000E-10
     Energy threshold:              0.1000E-10
     Max number of iterations:      100

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (cc_gs_diis_e): file
     Storage (cc_gs_diis_x): file

  Iteration    Energy (a.u.)        |omega|       Delta E (a.u.)
  ---------------------------------------------------------------
    1           -79.084241931608     0.9391E-01     0.7908E+02
    2           -79.092586666607     0.2720E-01     0.8345E-02
    3           -79.099408028306     0.7507E-02     0.6821E-02
    4           -79.100345871309     0.2095E-02     0.9378E-03
    5           -79.100371860301     0.5154E-03     0.2599E-04
    6           -79.100393801016     0.2313E-03     0.2194E-04
    7           -79.100385611260     0.4933E-04     0.8190E-05
    8           -79.100384217524     0.1180E-04     0.1394E-05
    9           -79.100383621794     0.4135E-05     0.5957E-06
   10           -79.100383427012     0.1779E-05     0.1948E-06
   11           -79.100383466393     0.6705E-06     0.3938E-07
   12           -79.100383474630     0.2989E-06     0.8237E-08
   13           -79.100383487351     0.1028E-06     0.1272E-07
   14           -79.100383481865     0.3056E-07     0.5487E-08
   15           -79.100383481092     0.6508E-08     0.7730E-09
   16           -79.100383481302     0.2112E-08     0.2109E-09
   17           -79.100383481484     0.6588E-09     0.1819E-09
   18           -79.100383481546     0.1594E-09     0.6158E-10
   19           -79.100383481554     0.5326E-10     0.7901E-11
   20           -79.100383481555     0.2008E-10     0.1208E-11
   21           -79.100383481556     0.7516E-11     0.6679E-12
  ---------------------------------------------------------------
  Convergence criterion met in 21 iterations!

  - Ground state summary:

     Final ground state energy (a.u.):   -79.100383481556

     Correlation energy (a.u.):           -0.256531787925

     Largest single amplitudes:
     -----------------------------------
        a       i         t(a,i)
     -----------------------------------
        1      5        0.014740597524
       14      4       -0.009546856220
        7      4        0.008284826483
       15      5       -0.006124828873
        4      5        0.005606072698
        6      2        0.005476844297
        2      4        0.005318591701
       13      5        0.005269818337
        5      6        0.004933006906
       11      6       -0.003454309399
     ------------------------------------

     Largest double amplitudes:
     --------------------------------------------------
        a      i       b      j         t(ai,bj)
     --------------------------------------------------
        2      4       2      4       -0.047351708919
        5      6       5      6       -0.046240574404
        9      3       9      3       -0.041367012248
        3      4       3      4       -0.036659067517
        6      5       6      5       -0.034554012170
        1      5       1      5       -0.034177347751
       16      3      16      3       -0.032108235347
       17      3      17      3       -0.032052553603
       18      3      18      3       -0.031351828684
        2      4       3      4       -0.029701270698
     --------------------------------------------------

  - Finished solving the CCSD ground state equations

     Total wall time (sec):              0.20188
     Total cpu time (sec):               0.40148


  4) Calculation of the excited state (davidson algorithm)
     Calculating right vectors

   - Davidson coupled cluster excited state solver
  ---------------------------------------------------

  A Davidson solver that calculates the lowest eigenvalues and the right 
  or left eigenvectors of the Jacobian matrix, A. The eigenvalue problem 
  is solved in a reduced space, the dimension of which is expanded until 
  the convergence criteria are met.

  A complete description of the algorithm can be found in E. R. Davidson, 
  J. Comput. Phys. 17, 87 (1975).

  - Settings for coupled cluster excited state solver (Davidson):

     Calculation type:    valence
     Excitation vectors:  right

     Residual threshold:            0.1000E-10
     Energy threshold:              0.1000E-10

     Number of singlet states:               3
     Max number of iterations:             100

   - Davidson tool settings:

     Number of parameters:                 9729
     Number of requested solutions:           3
     Max reduced space dimension:           100

     Storage (cc_es_davidson_trials): file
     Storage (cc_es_davidson_transforms): file

  Iteration:                  1
  Reduced space dimension:    3

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.378021714176    0.000000000000     0.4419E+00   0.3780E+00
     2   0.449071436599    0.000000000000     0.4454E+00   0.4491E+00
     3   0.485447321168    0.000000000000     0.4256E+00   0.4854E+00
  ------------------------------------------------------------------------

  Iteration:                  2
  Reduced space dimension:    6

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247587872712    0.000000000000     0.8953E-01   0.1304E+00
     2   0.314685589305    0.000000000000     0.1020E+00   0.1344E+00
     3   0.361637551474    0.000000000000     0.1093E+00   0.1238E+00
  ------------------------------------------------------------------------

  Iteration:                  3
  Reduced space dimension:    9

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.246599724037    0.000000000000     0.3235E-01   0.9881E-03
     2   0.312360467464    0.000000000000     0.4161E-01   0.2325E-02
     3   0.355506802266    0.000000000000     0.4270E-01   0.6131E-02
  ------------------------------------------------------------------------

  Iteration:                  4
  Reduced space dimension:   12

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247219879264    0.000000000000     0.9662E-02   0.6202E-03
     2   0.312948613923    0.000000000000     0.1363E-01   0.5881E-03
     3   0.356567905257    0.000000000000     0.1455E-01   0.1061E-02
  ------------------------------------------------------------------------

  Iteration:                  5
  Reduced space dimension:   15

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247240750153    0.000000000000     0.1980E-02   0.2087E-04
     2   0.312819462210    0.000000000000     0.3311E-02   0.1292E-03
     3   0.356116506763    0.000000000000     0.3899E-02   0.4514E-03
  ------------------------------------------------------------------------

  Iteration:                  6
  Reduced space dimension:   18

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247195018702    0.000000000000     0.5489E-03   0.4573E-04
     2   0.312713702021    0.000000000000     0.9558E-03   0.1058E-03
     3   0.356092342275    0.000000000000     0.1591E-02   0.2416E-04
  ------------------------------------------------------------------------

  Iteration:                  7
  Reduced space dimension:   21

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247193454028    0.000000000000     0.1648E-03   0.1565E-05
     2   0.312713934408    0.000000000000     0.2938E-03   0.2324E-06
     3   0.356078554993    0.000000000000     0.5581E-03   0.1379E-04
  ------------------------------------------------------------------------

  Iteration:                  8
  Reduced space dimension:   24

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194063074    0.000000000000     0.5256E-04   0.6090E-06
     2   0.312716904504    0.000000000000     0.1701E-03   0.2970E-05
     3   0.356089302961    0.000000000000     0.1841E-03   0.1075E-04
  ------------------------------------------------------------------------

  Iteration:                  9
  Reduced space dimension:   27

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194185657    0.000000000000     0.1325E-04   0.1226E-06
     2   0.312717231429    0.000000000000     0.1268E-03   0.3269E-06
     3   0.356084879759    0.000000000000     0.5903E-04   0.4423E-05
  ------------------------------------------------------------------------

  Iteration:                 10
  Reduced space dimension:   30

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194225723    0.000000000000     0.3520E-05   0.4007E-07
     2   0.312716888841    0.000000000000     0.6627E-04   0.3426E-06
     3   0.356084999451    0.000000000000     0.2221E-04   0.1197E-06
  ------------------------------------------------------------------------

  Iteration:                 11
  Reduced space dimension:   33

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194247675    0.000000000000     0.1049E-05   0.2195E-07
     2   0.312716762474    0.000000000000     0.2096E-04   0.1264E-06
     3   0.356085045748    0.000000000000     0.6691E-05   0.4630E-07
  ------------------------------------------------------------------------

  Iteration:                 12
  Reduced space dimension:   36

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194254931    0.000000000000     0.2883E-06   0.7256E-08
     2   0.312716685456    0.000000000000     0.6965E-05   0.7702E-07
     3   0.356085061344    0.000000000000     0.2076E-05   0.1560E-07
  ------------------------------------------------------------------------

  Iteration:                 13
  Reduced space dimension:   39

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255972    0.000000000000     0.9404E-07   0.1041E-08
     2   0.312716666729    0.000000000000     0.2741E-05   0.1873E-07
     3   0.356085054839    0.000000000000     0.6436E-06   0.6504E-08
  ------------------------------------------------------------------------

  Iteration:                 14
  Reduced space dimension:   42

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255450    0.000000000000     0.3221E-07   0.5223E-09
     2   0.312716689486    0.000000000000     0.1223E-05   0.2276E-07
     3   0.356085056850    0.000000000000     0.2012E-06   0.2010E-08
  ------------------------------------------------------------------------

  Iteration:                 15
  Reduced space dimension:   45

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255409    0.000000000000     0.9043E-08   0.4057E-10
     2   0.312716703271    0.000000000000     0.4251E-06   0.1378E-07
     3   0.356085058378    0.000000000000     0.7927E-07   0.1529E-08
  ------------------------------------------------------------------------

  Iteration:                 16
  Reduced space dimension:   48

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255538    0.000000000000     0.2152E-08   0.1286E-09
     2   0.312716700997    0.000000000000     0.1217E-06   0.2273E-08
     3   0.356085057752    0.000000000000     0.2878E-07   0.6265E-09
  ------------------------------------------------------------------------

  Iteration:                 17
  Reduced space dimension:   51

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255567    0.000000000000     0.5073E-09   0.2888E-10
     2   0.312716699012    0.000000000000     0.3424E-07   0.1985E-08
     3   0.356085057564    0.000000000000     0.8283E-08   0.1874E-09
  ------------------------------------------------------------------------

  Iteration:                 18
  Reduced space dimension:   54

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255572    0.000000000000     0.1413E-09   0.5326E-11
     2   0.312716698735    0.000000000000     0.1035E-07   0.2776E-09
     3   0.356085057581    0.000000000000     0.2218E-08   0.1694E-10
  ------------------------------------------------------------------------

  Iteration:                 19
  Reduced space dimension:   57

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255572    0.000000000000     0.3986E-10   0.4832E-12
     2   0.312716698743    0.000000000000     0.3590E-08   0.8106E-11
     3   0.356085057593    0.000000000000     0.6355E-09   0.1152E-10
  ------------------------------------------------------------------------

  Iteration:                 20
  Reduced space dimension:   60

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255572    0.000000000000     0.1125E-10   0.1717E-12
     2   0.312716698769    0.000000000000     0.1127E-08   0.2651E-10
     3   0.356085057594    0.000000000000     0.1890E-09   0.7404E-12
  ------------------------------------------------------------------------

  Iteration:                 21
  Reduced space dimension:   63

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255572    0.000000000000     0.2785E-11   0.1829E-13
     2   0.312716698779    0.000000000000     0.2942E-09   0.1031E-10
     3   0.356085057593    0.000000000000     0.4896E-10   0.7937E-12
  ------------------------------------------------------------------------

  Iteration:                 22
  Reduced space dimension:   65

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255572    0.000000000000     0.1498E-11   0.2609E-14
     2   0.312716698779    0.000000000000     0.7910E-10   0.2288E-12
     3   0.356085057592    0.000000000000     0.1133E-10   0.2692E-12
  ------------------------------------------------------------------------

  Iteration:                 23
  Reduced space dimension:   67

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255572    0.000000000000     0.1435E-11   0.2193E-14
     2   0.312716698779    0.000000000000     0.2020E-10   0.1522E-12
     3   0.356085057592    0.000000000000     0.2560E-11   0.4524E-13
  ------------------------------------------------------------------------

  Iteration:                 24
  Reduced space dimension:   68

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255572    0.000000000000     0.1441E-11   0.2276E-14
     2   0.312716698779    0.000000000000     0.4941E-11   0.5218E-14
     3   0.356085057592    0.000000000000     0.2323E-11   0.7938E-14
  ------------------------------------------------------------------------
  Convergence criterion met in 24 iterations!

  - Excitation vector amplitudes:

     Electronic state nr. 1

     Energy (Hartree):                  0.247194255572
     Fraction singles (|R1|/|R|):       0.973397678083

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        1      6       -0.964249868485
        4      6       -0.121847295823
        6      6        0.036249387510
       13      6        0.030061485952
        1      3        0.011074241439
       22      6       -0.008603353606
       19      5        0.007863834135
        1      5       -0.007857920531
       10      6       -0.007675079118
        9      6        0.007427820492
     ------------------------------------

     Largest double amplitudes:
     --------------------------------------------------
        a      i       b      j         R(ai,bj)
     --------------------------------------------------
        2      4       1      6        0.100746791318
        1      5       1      6        0.085612058155
        1      6       5      6        0.066933008112
        1      2       1      6        0.063267573162
        1      4       2      6        0.061512599149
        3      4       1      6        0.054854109240
        6      5       1      6       -0.046260935197
        4      5       1      6        0.038260256937
        7      4       1      6       -0.032958804244
        4      4       2      6        0.032734134559
     --------------------------------------------------

     Electronic state nr. 2

     Energy (Hartree):                  0.312716698779
     Fraction singles (|R1|/|R|):       0.974191508731

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        2      6        0.954551201852
        3      6        0.178658232264
        7      6       -0.072428436855
       14      6        0.020845095882
        2      3       -0.011040706129
       12      6       -0.005883373005
        8      2       -0.005692890963
        2      5        0.005144277710
        5      4       -0.004935182404
       11      4       -0.003612992496
     ------------------------------------

     Largest double amplitudes:
     --------------------------------------------------
        a      i       b      j         R(ai,bj)
     --------------------------------------------------
        2      4       2      6       -0.110664929270
        1      5       2      6       -0.079354628531
        2      6       5      6       -0.060325722580
        2      5       1      6       -0.050061414585
        3      4       2      6       -0.047564612585
        1      4       1      6       -0.045251222953
        1      2       2      6       -0.045012448109
        4      5       2      6       -0.039742197636
        6      5       2      6        0.038941412498
        3      5       1      6       -0.034289057215
     --------------------------------------------------

     Electronic state nr. 3

     Energy (Hartree):                  0.356085057592
     Fraction singles (|R1|/|R|):       0.977805372200

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        1      5        0.957314847871
        2      4       -0.137091155508
        4      5        0.090812850214
        1      2       -0.071675967623
        3      4       -0.054796039254
        5      6       -0.040602500552
       13      5       -0.029458132227
        7      4        0.025825254324
        4      2       -0.021074995285
       10      5        0.013972713306
     ------------------------------------

     Largest double amplitudes:
     --------------------------------------------------
        a      i       b      j         R(ai,bj)
     --------------------------------------------------
        2      4       1      5       -0.082435001503
        1      5       1      5       -0.081686786865
        1      5       6      5        0.054811008558
        1      4       2      5       -0.048159897951
        1      5       4      5       -0.042695435568
        3      4       1      5       -0.039841925826
        1      5       5      6       -0.039669396138
        2      5       3      5       -0.037629696693
        1      2       3      4        0.032609865471
        4      4       2      5       -0.031072898992
     --------------------------------------------------

     - Electronic excitation energies:

                                      Excitation energy
                          ------------------------------------------
      State                (Hartree)             (eV)
     ---------------------------------------------------------------
        1                  0.247194255572        6.726498310303
        2                  0.312716698779        8.509454805378
        3                  0.356085057592        9.689567958102
     ---------------------------------------------------------------
     eV/Hartree (CODATA 2014): 27.21138602

  - Finished solving the CCSD excited state equations (right)

     Total wall time (sec):              0.45453
     Total cpu time (sec):               0.90841

  - Timings for the CCSD excited state calculation

     Total wall time (sec):              0.77658
     Total cpu time (sec):               1.55069

  ------------------------------------------------------------

  Peak memory usage during the execution of eT: 16.462184 MB

  Total wall time in eT (sec):              1.12383
  Total cpu time in eT (sec):               2.15660

  Calculation ended: 2021-06-07 12:50:55 UTC +02:00

  - Implementation references:

     eT: https://doi.org/10.1063/5.0004713
     Cholesky decomposition of ERIs: https://doi.org/10.1063/1.5083802

  eT terminated successfully!

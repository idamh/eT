


                     eT 1.3 - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, T. Moitra, R. H. Myhre, A. C. Paul, S. Roet, M. Scavino, 
   A. Skeidsvoll, Å. H. Tveten
  ------------------------------------------------------------------------
   J. Chem. Phys. 152, 184103 (2020); https://doi.org/10.1063/5.0004713


   This is eT 1.3.0 Disco
  ------------------------------------------------------------
  Configuration date: 2021-06-07 12:48:33 UTC +02:00
  Git branch:         update-test-refs-new
  Git hash:           07052c810cfb89ed77f5f7b0b9d39be404f0338e
  Fortran compiler:   GNU 10.3.0
  C compiler:         GNU 10.3.0
  C++ compiler:       GNU 10.3.0
  LAPACK type:        MKL
  BLAS type:          MKL
  64-bit integers:    ON
  OpenMP:             ON
  PCM:                ON
  Forced batching:    OFF
  Runtime checks:     OFF
  ------------------------------------------------------------

  Calculation started: 2021-06-07 12:50:23 UTC +02:00


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: H2O He
        charge: 0
     end system

     do
        ground state
        excited state
     end do

     memory
        available: 8
     end memory

     solver cholesky
        threshold: 1.0d-12
     end solver cholesky

     solver scf
        algorithm:          scf-diis
        energy threshold:   1.0d-10
        gradient threshold: 1.0d-10
     end solver scf

     method
        hf
        ccsd
     end method

     solver cc gs
        omega threshold:  1.0d-10
        energy threshold: 1.0d-10
     end solver cc gs

     solver cc es
        algorithm:          davidson
        singlet states:     2
        residual threshold: 1.0d-10
        energy threshold:   1.0d-10
        right eigenvectors
        max reduced dimension: 20
     end solver cc es


  Running on 2 OMP threads
  Memory available for calculation: 8.000000 GB


  :: RHF wavefunction
  ======================

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  H     0.866810000000     0.601440000000     5.000000000000        1
        2  H    -0.866810000000     0.601440000000     5.000000000000        2
        3  O     0.000000000000    -0.075790000000     5.000000000000        3
        4 He     0.100000000000    -0.020000000000     7.530000000000        4
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  H     1.638033502034     1.136556880358     9.448630622825        1
        2  H    -1.638033502034     1.136556880358     9.448630622825        2
        3  O     0.000000000000    -0.143222342981     9.448630622825        3
        4 He     0.188972612457    -0.037794522491    14.229637717975        4
     ==============================================================================

  - Cholesky decomposition of AO overlap to get linearly independent AOs:

     Linear dependence threshold:             0.10E-05
     Number of atomic orbitals:               29
     Number of orthonormal atomic orbitals:   29

  - Molecular orbital details:

     Number of occupied orbitals:         6
     Number of virtual orbitals:         23
     Number of molecular orbitals:       29


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a RHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)


  1) Generate initial SAD density


  2) Calculation of reference state (SCF-DIIS algorithm)

  - SCF solver settings:

     Maximum iterations:                   100
     Acceleration type:                   diis

     Residual threshold:            0.1000E-09
     Energy threshold:              0.1000E-09

  - Setting initial AO density to sad

     Energy of initial guess:               -78.492022836338
     Number of electrons in guess:           12.000000000000

  - Screening and integral thresholds:

     Coulomb screening threshold:    0.1000E-15
     Exchange screening threshold:   0.1000E-13
     ERI cutoff:                     0.1000E-15
     One-electron integral  cutoff:  0.1000E-20
     Cumulative Fock threshold:      0.1000E+01

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (solver scf_e): memory
     Storage (solver scf_x): memory

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1           -78.796606592630     0.9053E-01     0.7880E+02
     2           -78.828675852673     0.7128E-01     0.3207E-01
     3           -78.843428154462     0.8290E-02     0.1475E-01
     4           -78.843809541675     0.2414E-02     0.3814E-03
     5           -78.843850846799     0.3301E-03     0.4131E-04
     6           -78.843851669326     0.5527E-04     0.8225E-06
     7           -78.843851692851     0.5865E-05     0.2353E-07
     8           -78.843851693557     0.2356E-05     0.7054E-09
     9           -78.843851693630     0.2071E-06     0.7377E-10
    10           -78.843851693631     0.1594E-07     0.4263E-12
    11           -78.843851693631     0.3322E-08     0.1421E-13
    12           -78.843851693631     0.1197E-08     0.0000E+00
    13           -78.843851693631     0.4264E-09     0.1421E-13
    14           -78.843851693631     0.1280E-09     0.0000E+00
    15           -78.843851693631     0.1479E-10     0.1421E-13
  ---------------------------------------------------------------
  Convergence criterion met in 15 iterations!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.645629080236
     Nuclear repulsion energy:      12.116100574587
     Electronic energy:            -90.959952268218
     Total energy:                 -78.843851693631

  - Timings for the RHF ground state calculation

     Total wall time (sec):              0.32185
     Total cpu time (sec):               0.56832


  :: CCSD wavefunction
  =======================

     Bath orbital(s):         False

   - Number of orbitals:

     Occupied orbitals:    6
     Virtual orbitals:     23
     Molecular orbitals:   29
     Atomic orbitals:      29

   - Number of ground state amplitudes:

     Single excitation amplitudes:  138
     Double excitation amplitudes:  9591


  :: Excited state coupled cluster engine
  ==========================================

  Calculates the coupled cluster excitation vectors and excitation energies

  This is a CCSD excited state calculation.
  The following tasks will be performed:

     1) Cholesky decomposition of the electron repulsion integrals
     2) Preparation of MO basis and integrals
     3) Calculation of the ground state (diis algorithm)
     4) Calculation of the excited state (davidson algorithm)


  1) Cholesky decomposition of the electron repulsion integrals

   - Cholesky decomposition of electronic repulsion integrals solver
  ---------------------------------------------------------------------

  Performs a Cholesky decomposition of the two-electron electronic repulsion 
  integrals in the atomic orbital basis,

  (ab|cd) = sum_J L_ab^J L_cd^J.

  Once the Cholesky basis has been determined, the vectors L^J are constructed 
  and stored to disk. These may either be used directly, or be transformed 
  to the MO basis for use in post-HF calculations. For more information, 
  see S. D. Folkestad, E. F. Kjønstad and H. Koch, JCP, 150(19), (2019)

  - Cholesky decomposition settings:

     Target threshold is:   0.10E-11
     Span factor:           0.10E-01
     Max qual:                  1000

  - Cholesky decomposition ao details:

     Total number of AOs:                    29
     Total number of shell pairs:           120
     Total number of AO pairs:              435

     Significant shell pairs:               118
     Significant AO pairs:                  431

     Construct shell pairs:                 120
     Construct AO pairs:                    435

  Iter.  #Sign. ao pairs / shell pairs   Max diagonal    #Qualified    #Cholesky    Cholesky array size
  -------------------------------------------------------------------------------------------------------
     1               406 /     108       0.47383E+01         147             42             17052
     2               319 /      93       0.47165E-01         234            111             35409
     3               250 /      76       0.46944E-03         178            183             45750
     4               187 /      55       0.38270E-05         145            265             49555
     5                87 /      25       0.38106E-07          78            324             28188
     6                21 /       3       0.37202E-09          43            349              7329
     7                 0 /       0       0.36652E-11           7            354                 0
  -------------------------------------------------------------------------------------------------------

  - Summary of Cholesky decomposition of electronic repulsion integrals:

     Final number of Cholesky vectors: 354

 - Testing the Cholesky decomposition decomposition electronic repulsion integrals:

     Maximal difference between approximate and actual diagonal:              0.7161E-12
     Minimal element of difference between approximate and actual diagonal:  -0.1238E-14

  - Settings for integral handling:

     Cholesky vectors in memory: True
     ERI matrix in memory:       False
     T1 ERI matrix in memory:    True

  - Finished decomposing the ERIs.

     Total wall time (sec):              0.12555
     Total cpu time (sec):               0.25306


  2) Preparation of MO basis and integrals


  3) Calculation of the ground state (diis algorithm)

   - DIIS coupled cluster ground state solver
  ----------------------------------------------

  A DIIS CC ground state amplitude equations solver. It uses an extrapolation 
  of previous quasi-Newton perturbation theory estimates of the next amplitudes. 
  See Helgaker et al., Molecular Electronic Structure Theory, Chapter 
  13.

  - CC ground state solver settings:

     Residual threshold:            0.1000E-09
     Energy threshold:              0.1000E-09
     Max number of iterations:      100

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (cc_gs_diis_e): file
     Storage (cc_gs_diis_x): file

  Iteration    Energy (a.u.)        |omega|       Delta E (a.u.)
  ---------------------------------------------------------------
    1           -79.084241931609     0.9391E-01     0.7908E+02
    2           -79.092586666608     0.2720E-01     0.8345E-02
    3           -79.099408028306     0.7507E-02     0.6821E-02
    4           -79.100345871309     0.2095E-02     0.9378E-03
    5           -79.100371860301     0.5154E-03     0.2599E-04
    6           -79.100393801016     0.2313E-03     0.2194E-04
    7           -79.100385611260     0.4933E-04     0.8190E-05
    8           -79.100384217524     0.1180E-04     0.1394E-05
    9           -79.100383621795     0.4135E-05     0.5957E-06
   10           -79.100383427012     0.1779E-05     0.1948E-06
   11           -79.100383466393     0.6705E-06     0.3938E-07
   12           -79.100383474630     0.2989E-06     0.8237E-08
   13           -79.100383487352     0.1028E-06     0.1272E-07
   14           -79.100383481865     0.3056E-07     0.5487E-08
   15           -79.100383481092     0.6508E-08     0.7730E-09
   16           -79.100383481303     0.2112E-08     0.2109E-09
   17           -79.100383481485     0.6588E-09     0.1819E-09
   18           -79.100383481546     0.1594E-09     0.6158E-10
   19           -79.100383481554     0.5326E-10     0.7915E-11
  ---------------------------------------------------------------
  Convergence criterion met in 19 iterations!

  - Ground state summary:

     Final ground state energy (a.u.):   -79.100383481554

     Correlation energy (a.u.):           -0.256531787923

     Largest single amplitudes:
     -----------------------------------
        a       i         t(a,i)
     -----------------------------------
        1      5        0.014740597486
       14      4       -0.009546856216
        7      4        0.008284826477
       15      5       -0.006124828872
        4      5        0.005606072685
        6      2        0.005476844297
        2      4        0.005318591723
       13      5        0.005269818340
        5      6        0.004933006894
       11      6       -0.003454309400
     ------------------------------------

     Largest double amplitudes:
     --------------------------------------------------
        a      i       b      j         t(ai,bj)
     --------------------------------------------------
        2      4       2      4       -0.047351708919
        5      6       5      6       -0.046240574399
        9      3       9      3       -0.041367012251
        3      4       3      4       -0.036659067518
        6      5       6      5       -0.034554012169
        1      5       1      5       -0.034177347752
       16      3      16      3       -0.032108235347
       17      3      17      3       -0.032052553602
       18      3      18      3       -0.031351828683
        2      4       3      4       -0.029701270697
     --------------------------------------------------

  - Finished solving the CCSD ground state equations

     Total wall time (sec):              0.18376
     Total cpu time (sec):               0.36773


  4) Calculation of the excited state (davidson algorithm)
     Calculating right vectors

   - Davidson coupled cluster excited state solver
  ---------------------------------------------------

  A Davidson solver that calculates the lowest eigenvalues and the right 
  or left eigenvectors of the Jacobian matrix, A. The eigenvalue problem 
  is solved in a reduced space, the dimension of which is expanded until 
  the convergence criteria are met.

  A complete description of the algorithm can be found in E. R. Davidson, 
  J. Comput. Phys. 17, 87 (1975).

  - Settings for coupled cluster excited state solver (Davidson):

     Calculation type:    valence
     Excitation vectors:  right

     Residual threshold:            0.1000E-09
     Energy threshold:              0.1000E-09

     Number of singlet states:               2
     Max number of iterations:             100

   - Davidson tool settings:

     Number of parameters:                 9729
     Number of requested solutions:           2
     Max reduced space dimension:            20

     Storage (cc_es_davidson_trials): file
     Storage (cc_es_davidson_transforms): file

  Iteration:                  1
  Reduced space dimension:    2

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.378021731473    0.000000000000     0.4419E+00   0.3780E+00
     2   0.485447187524    0.000000000000     0.4256E+00   0.4854E+00
  ------------------------------------------------------------------------

  Iteration:                  2
  Reduced space dimension:    4

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247587893297    0.000000000000     0.8953E-01   0.1304E+00
     2   0.361637524657    0.000000000000     0.1093E+00   0.1238E+00
  ------------------------------------------------------------------------

  Iteration:                  3
  Reduced space dimension:    6

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.246599742343    0.000000000000     0.3235E-01   0.9882E-03
     2   0.355506758363    0.000000000000     0.4270E-01   0.6131E-02
  ------------------------------------------------------------------------

  Iteration:                  4
  Reduced space dimension:    8

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247219896514    0.000000000000     0.9662E-02   0.6202E-03
     2   0.356567867806    0.000000000000     0.1455E-01   0.1061E-02
  ------------------------------------------------------------------------

  Iteration:                  5
  Reduced space dimension:   10

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247240765515    0.000000000000     0.1981E-02   0.2087E-04
     2   0.356116428174    0.000000000000     0.3900E-02   0.4514E-03
  ------------------------------------------------------------------------

  Iteration:                  6
  Reduced space dimension:   12

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247195004633    0.000000000000     0.5500E-03   0.4576E-04
     2   0.356092222391    0.000000000000     0.1597E-02   0.2421E-04
  ------------------------------------------------------------------------

  Iteration:                  7
  Reduced space dimension:   14

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247193433574    0.000000000000     0.1674E-03   0.1571E-05
     2   0.356078256948    0.000000000000     0.5745E-03   0.1397E-04
  ------------------------------------------------------------------------

  Iteration:                  8
  Reduced space dimension:   16

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194072581    0.000000000000     0.5920E-04   0.6390E-06
     2   0.356090221756    0.000000000000     0.2513E-03   0.1196E-04
  ------------------------------------------------------------------------

  Iteration:                  9
  Reduced space dimension:   18

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194227856    0.000000000000     0.3833E-04   0.1553E-06
     2   0.356076775058    0.000000000000     0.4949E-03   0.1345E-04
  ------------------------------------------------------------------------

  Iteration:                 10
  Reduced space dimension:   20

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194279812    0.000000000000     0.2668E-04   0.5196E-07
     2   0.327798998064    0.000000000000     0.1662E+00   0.2828E-01
  ------------------------------------------------------------------------

  Iteration:                 11
  Reduced space dimension:    2

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194279812    0.000000000000     0.2668E-04   0.7772E-15
     2   0.327798998064    0.000000000000     0.1662E+00   0.1610E-14
  ------------------------------------------------------------------------

  Iteration:                 12
  Reduced space dimension:    4

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194254058    0.000000000000     0.1066E-04   0.2575E-07
     2   0.315167961744    0.000000000000     0.5944E-01   0.1263E-01
  ------------------------------------------------------------------------

  Iteration:                 13
  Reduced space dimension:    6

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194264337    0.000000000000     0.3958E-05   0.1028E-07
     2   0.312539387139    0.000000000000     0.2154E-01   0.2629E-02
  ------------------------------------------------------------------------

  Iteration:                 14
  Reduced space dimension:    8

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194251899    0.000000000000     0.1427E-05   0.1244E-07
     2   0.312969712092    0.000000000000     0.8996E-02   0.4303E-03
  ------------------------------------------------------------------------

  Iteration:                 15
  Reduced space dimension:   10

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255454    0.000000000000     0.6365E-06   0.3555E-08
     2   0.312775194570    0.000000000000     0.4990E-02   0.1945E-03
  ------------------------------------------------------------------------

  Iteration:                 16
  Reduced space dimension:   12

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194256824    0.000000000000     0.3378E-06   0.1371E-08
     2   0.312761559794    0.000000000000     0.3313E-02   0.1363E-04
  ------------------------------------------------------------------------

  Iteration:                 17
  Reduced space dimension:   14

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255728    0.000000000000     0.1582E-06   0.1096E-08
     2   0.312715248414    0.000000000000     0.2044E-02   0.4631E-04
  ------------------------------------------------------------------------

  Iteration:                 18
  Reduced space dimension:   16

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255412    0.000000000000     0.5267E-07   0.3165E-09
     2   0.312705512699    0.000000000000     0.8935E-03   0.9736E-05
  ------------------------------------------------------------------------

  Iteration:                 19
  Reduced space dimension:   18

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255434    0.000000000000     0.1667E-07   0.2177E-10
     2   0.312721101873    0.000000000000     0.4867E-03   0.1559E-04
  ------------------------------------------------------------------------

  Iteration:                 20
  Reduced space dimension:   20

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255566    0.000000000000     0.6267E-08   0.1324E-09
     2   0.312714808298    0.000000000000     0.2629E-03   0.6294E-05
  ------------------------------------------------------------------------

  Iteration:                 21
  Reduced space dimension:    2

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255566    0.000000000000     0.6267E-08   0.4163E-15
     2   0.312714808298    0.000000000000     0.2629E-03   0.1277E-14
  ------------------------------------------------------------------------

  Iteration:                 22
  Reduced space dimension:    4

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255571    0.000000000000     0.2151E-08   0.5037E-11
     2   0.312717900203    0.000000000000     0.1030E-03   0.3092E-05
  ------------------------------------------------------------------------

  Iteration:                 23
  Reduced space dimension:    6

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255566    0.000000000000     0.1009E-08   0.5462E-11
     2   0.312716148504    0.000000000000     0.5744E-04   0.1752E-05
  ------------------------------------------------------------------------

  Iteration:                 24
  Reduced space dimension:    8

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255569    0.000000000000     0.3770E-09   0.2998E-11
     2   0.312716547824    0.000000000000     0.2299E-04   0.3993E-06
  ------------------------------------------------------------------------

  Iteration:                 25
  Reduced space dimension:   10

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255571    0.000000000000     0.2131E-09   0.2103E-11
     2   0.312716932882    0.000000000000     0.1112E-04   0.3851E-06
  ------------------------------------------------------------------------

  Iteration:                 26
  Reduced space dimension:   12

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255570    0.000000000000     0.9976E-10   0.8840E-12
     2   0.312716701129    0.000000000000     0.4909E-05   0.2318E-06
  ------------------------------------------------------------------------

  Iteration:                 27
  Reduced space dimension:   13

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255569    0.000000000000     0.8318E-10   0.8165E-12
     2   0.312716680906    0.000000000000     0.1875E-05   0.2022E-07
  ------------------------------------------------------------------------

  Iteration:                 28
  Reduced space dimension:   14

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255569    0.000000000000     0.8079E-10   0.5632E-13
     2   0.312716706193    0.000000000000     0.9374E-06   0.2529E-07
  ------------------------------------------------------------------------

  Iteration:                 29
  Reduced space dimension:   15

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255569    0.000000000000     0.8120E-10   0.1149E-13
     2   0.312716693668    0.000000000000     0.7079E-06   0.1252E-07
  ------------------------------------------------------------------------

  Iteration:                 30
  Reduced space dimension:   16

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255569    0.000000000000     0.8083E-10   0.4324E-13
     2   0.312716694455    0.000000000000     0.3504E-06   0.7873E-09
  ------------------------------------------------------------------------

  Iteration:                 31
  Reduced space dimension:   17

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255569    0.000000000000     0.7897E-10   0.6223E-13
     2   0.312716701103    0.000000000000     0.1603E-06   0.6647E-08
  ------------------------------------------------------------------------

  Iteration:                 32
  Reduced space dimension:   18

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255569    0.000000000000     0.7876E-10   0.6606E-14
     2   0.312716699185    0.000000000000     0.5653E-07   0.1917E-08
  ------------------------------------------------------------------------

  Iteration:                 33
  Reduced space dimension:   19

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255569    0.000000000000     0.7914E-10   0.4219E-14
     2   0.312716698819    0.000000000000     0.1863E-07   0.3661E-09
  ------------------------------------------------------------------------

  Iteration:                 34
  Reduced space dimension:   20

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255569    0.000000000000     0.7890E-10   0.3569E-13
     2   0.312716698763    0.000000000000     0.5603E-08   0.5662E-10
  ------------------------------------------------------------------------

  Iteration:                 35
  Reduced space dimension:    2

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255569    0.000000000000     0.7890E-10   0.8327E-15
     2   0.312716698763    0.000000000000     0.5603E-08   0.6106E-15
  ------------------------------------------------------------------------

  Iteration:                 36
  Reduced space dimension:    3

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255570    0.000000000000     0.6551E-10   0.3763E-12
     2   0.312716698779    0.000000000000     0.1853E-08   0.1584E-10
  ------------------------------------------------------------------------

  Iteration:                 37
  Reduced space dimension:    4

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255570    0.000000000000     0.6550E-10   0.8882E-15
     2   0.312716698785    0.000000000000     0.8425E-09   0.6292E-11
  ------------------------------------------------------------------------

  Iteration:                 38
  Reduced space dimension:    5

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255570    0.000000000000     0.6581E-10   0.3864E-13
     2   0.312716698779    0.000000000000     0.2781E-09   0.5916E-11
  ------------------------------------------------------------------------

  Iteration:                 39
  Reduced space dimension:    6

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255570    0.000000000000     0.6690E-10   0.5729E-13
     2   0.312716698776    0.000000000000     0.2001E-09   0.3426E-11
  ------------------------------------------------------------------------

  Iteration:                 40
  Reduced space dimension:    7

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255570    0.000000000000     0.6492E-10   0.2798E-13
     2   0.312716698777    0.000000000000     0.2019E-09   0.9555E-12
  ------------------------------------------------------------------------

  Iteration:                 41
  Reduced space dimension:    8

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255570    0.000000000000     0.6480E-10   0.1017E-12
     2   0.312716698777    0.000000000000     0.1292E-09   0.3170E-12
  ------------------------------------------------------------------------

  Iteration:                 42
  Reduced space dimension:    9

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255570    0.000000000000     0.6375E-10   0.1910E-13
     2   0.312716698776    0.000000000000     0.6512E-10   0.6086E-12
  ------------------------------------------------------------------------
  Convergence criterion met in 42 iterations!

  - Excitation vector amplitudes:

     Electronic state nr. 1

     Energy (Hartree):                  0.247194255570
     Fraction singles (|R1|/|R|):       0.973397678084

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        1      6       -0.964249868486
        4      6       -0.121847295827
        6      6        0.036249387512
       13      6        0.030061485949
        1      3        0.011074241438
       22      6       -0.008603353606
       19      5        0.007863834136
        1      5       -0.007857920546
       10      6       -0.007675079112
        9      6        0.007427820489
     ------------------------------------

     Largest double amplitudes:
     --------------------------------------------------
        a      i       b      j         R(ai,bj)
     --------------------------------------------------
        2      4       1      6        0.100746791315
        1      5       1      6        0.085612058152
        1      6       5      6        0.066933008109
        1      2       1      6        0.063267573161
        1      4       2      6        0.061512599148
        3      4       1      6        0.054854109240
        6      5       1      6       -0.046260935199
        4      5       1      6        0.038260256938
        7      4       1      6       -0.032958804246
        4      4       2      6        0.032734134559
     --------------------------------------------------

     Electronic state nr. 2

     Energy (Hartree):                  0.312716698776
     Fraction singles (|R1|/|R|):       0.974191508733

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        2      6        0.954551201854
        3      6        0.178658232258
        7      6       -0.072428436859
       14      6        0.020845095880
        2      3       -0.011040706128
       12      6       -0.005883373008
        8      2       -0.005692890963
        2      5        0.005144277858
        5      4       -0.004935182403
       11      4       -0.003612992494
     ------------------------------------

     Largest double amplitudes:
     --------------------------------------------------
        a      i       b      j         R(ai,bj)
     --------------------------------------------------
        2      4       2      6       -0.110664929271
        1      5       2      6       -0.079354628520
        2      6       5      6       -0.060325722579
        2      5       1      6       -0.050061414579
        3      4       2      6       -0.047564612587
        1      4       1      6       -0.045251222949
        1      2       2      6       -0.045012448109
        4      5       2      6       -0.039742197634
        6      5       2      6        0.038941412498
        3      5       1      6       -0.034289057213
     --------------------------------------------------

     - Electronic excitation energies:

                                      Excitation energy
                          ------------------------------------------
      State                (Hartree)             (eV)
     ---------------------------------------------------------------
        1                  0.247194255570        6.726498310229
        2                  0.312716698776        8.509454805300
     ---------------------------------------------------------------
     eV/Hartree (CODATA 2014): 27.21138602

  - Finished solving the CCSD excited state equations (right)

     Total wall time (sec):              0.37347
     Total cpu time (sec):               0.74523

  - Timings for the CCSD excited state calculation

     Total wall time (sec):              0.68315
     Total cpu time (sec):               1.36640

  ------------------------------------------------------------

  Peak memory usage during the execution of eT: 16.353888 MB

  Total wall time in eT (sec):              1.02662
  Total cpu time in eT (sec):               1.96022

  Calculation ended: 2021-06-07 12:50:24 UTC +02:00

  - Implementation references:

     eT: https://doi.org/10.1063/5.0004713
     Cholesky decomposition of ERIs: https://doi.org/10.1063/1.5083802

  eT terminated successfully!

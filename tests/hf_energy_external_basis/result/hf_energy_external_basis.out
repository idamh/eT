


                     eT 1.3 - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, T. Moitra, R. H. Myhre, A. C. Paul, S. Roet, M. Scavino, 
   A. Skeidsvoll, Å. H. Tveten
  ------------------------------------------------------------------------
   J. Chem. Phys. 152, 184103 (2020); https://doi.org/10.1063/5.0004713


   This is eT 1.3.0 Disco
  ------------------------------------------------------------
  Configuration date: 2021-06-07 12:48:33 UTC +02:00
  Git branch:         update-test-refs-new
  Git hash:           07052c810cfb89ed77f5f7b0b9d39be404f0338e
  Fortran compiler:   GNU 10.3.0
  C compiler:         GNU 10.3.0
  C++ compiler:       GNU 10.3.0
  LAPACK type:        MKL
  BLAS type:          MKL
  64-bit integers:    ON
  OpenMP:             ON
  PCM:                ON
  Forced batching:    OFF
  Runtime checks:     OFF
  ------------------------------------------------------------

  Calculation started: 2021-06-07 12:49:30 UTC +02:00


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: HOF He
        charge: 0
        cartesian gaussians
     end system

     do
        ground state
     end do

     memory
        available: 8
     end memory

     solver scf
        algorithm:          scf-diis
        energy threshold:   1.0d-10
        gradient threshold: 1.0d-10
     end solver scf

     method
        hf
     end method


  Running on 2 OMP threads
  Memory available for calculation: 8.000000 GB


  :: RHF wavefunction
  ======================

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: my_own_basis
        1  H     0.866810000000     0.601440000000     5.000000000000        1
        2  F    -0.866810000000     0.601440000000     5.000000000000        2
        3  O     0.000000000000    -0.075790000000     5.000000000000        3
        4 He     0.000000000000     0.000000000000     7.500000000000        4
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: my_own_basis
        1  H     1.638033502034     1.136556880358     9.448630622825        1
        2  F    -1.638033502034     1.136556880358     9.448630622825        2
        3  O     0.000000000000    -0.143222342981     9.448630622825        3
        4 He     0.000000000000     0.000000000000    14.172945934238        4
     ==============================================================================

  - Cholesky decomposition of AO overlap to get linearly independent AOs:

     Linear dependence threshold:             0.10E-05
     Number of atomic orbitals:               40
     Number of orthonormal atomic orbitals:   40

  - Molecular orbital details:

     Number of occupied orbitals:        10
     Number of virtual orbitals:         30
     Number of molecular orbitals:       40


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a RHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)


  1) Generate initial SAD density


  2) Calculation of reference state (SCF-DIIS algorithm)

  - SCF solver settings:

     Maximum iterations:                   100
     Acceleration type:                   diis

     Residual threshold:            0.1000E-09
     Energy threshold:              0.1000E-09

  - Setting initial AO density to sad

     Energy of initial guess:              -178.317285807104
     Number of electrons in guess:           20.000000000000

  - Screening and integral thresholds:

     Coulomb screening threshold:    0.1000E-15
     Exchange screening threshold:   0.1000E-13
     ERI cutoff:                     0.1000E-15
     One-electron integral  cutoff:  0.1000E-20
     Cumulative Fock threshold:      0.1000E+01

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (solver scf_e): memory
     Storage (solver scf_x): memory

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1          -177.453805426822     0.4365E-01     0.1775E+03
     2          -177.480689608041     0.1133E-01     0.2688E-01
     3          -177.481709170233     0.4391E-02     0.1020E-02
     4          -177.481831291739     0.1861E-02     0.1221E-03
     5          -177.481845339604     0.3384E-03     0.1405E-04
     6          -177.481846431524     0.7551E-04     0.1092E-05
     7          -177.481846505481     0.2092E-04     0.7396E-07
     8          -177.481846516386     0.4353E-05     0.1090E-07
     9          -177.481846517063     0.1142E-05     0.6765E-09
    10          -177.481846517088     0.1674E-06     0.2569E-10
    11          -177.481846517089     0.3914E-07     0.5400E-12
    12          -177.481846517089     0.7333E-08     0.1137E-12
    13          -177.481846517089     0.2821E-08     0.5684E-13
    14          -177.481846517089     0.1276E-08     0.1705E-12
    15          -177.481846517089     0.3330E-09     0.1990E-12
    16          -177.481846517089     0.1042E-09     0.2842E-13
    17          -177.481846517089     0.3671E-10     0.0000E+00
  ---------------------------------------------------------------
  Convergence criterion met in 17 iterations!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.599166914110
     Nuclear repulsion energy:      48.518317619727
     Electronic energy:           -226.000164136816
     Total energy:                -177.481846517089

  - Timings for the RHF ground state calculation

     Total wall time (sec):              0.84562
     Total cpu time (sec):               1.63893

  ------------------------------------------------------------

  Peak memory usage during the execution of eT: 329.768 KB

  Total wall time in eT (sec):              0.85545
  Total cpu time in eT (sec):               1.65015

  Calculation ended: 2021-06-07 12:49:31 UTC +02:00

  - Implementation references:

     eT: https://doi.org/10.1063/5.0004713

  eT terminated successfully!

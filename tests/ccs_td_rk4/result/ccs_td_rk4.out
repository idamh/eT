


                     eT 1.3 - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, T. Moitra, R. H. Myhre, A. C. Paul, S. Roet, M. Scavino, 
   A. Skeidsvoll, Å. H. Tveten
  ------------------------------------------------------------------------
   J. Chem. Phys. 152, 184103 (2020); https://doi.org/10.1063/5.0004713


   This is eT 1.3.0 Disco
  ------------------------------------------------------------
  Configuration date: 2021-06-07 12:48:33 UTC +02:00
  Git branch:         update-test-refs-new
  Git hash:           07052c810cfb89ed77f5f7b0b9d39be404f0338e
  Fortran compiler:   GNU 10.3.0
  C compiler:         GNU 10.3.0
  C++ compiler:       GNU 10.3.0
  LAPACK type:        MKL
  BLAS type:          MKL
  64-bit integers:    ON
  OpenMP:             ON
  PCM:                ON
  Forced batching:    OFF
  Runtime checks:     OFF
  ------------------------------------------------------------

  Calculation started: 2021-06-07 12:49:50 UTC +02:00


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: H2O He
        charge: 0
     end system

     do
        time dependent state
     end do

     method
        hf
        ccs
     end method

     memory
        available: 8
     end memory

     solver cholesky
        threshold: 1.0d-12
     end solver cholesky

     solver scf
       algorithm:          scf-diis
       energy threshold:   1.0d-12
       gradient threshold: 1.0d-12
     end solver scf

     solver cc gs
        omega threshold:  1.0d-10
        energy threshold: 1.0d-10
     end solver cc gs

     cc td
        propagation
     end cc td

     solver cc multipliers
        algorithm: davidson
        threshold: 1.0d-10
     end solver cc multipliers

     solver cc propagation
        initial time: 0.0
        final time: 0.1
        time step: 0.02
        integrator: rk4
        energy output
        dipole moment output
        implicit threshold: 1.0d-10
        steps between output: 1
     end solver cc propagation

     electric field
         envelope: {2}
         x polarization: {0.0}
         y polarization: {0.0}
         z polarization: {1.0}
         central time: {0.1}
         width: {0.2}
         central angular frequency: {2.8735643}
         peak strength: {1.0}
         phase shift: {0.0}
     end electric field


  Running on 2 OMP threads
  Memory available for calculation: 8.000000 GB


  :: RHF wavefunction
  ======================

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  H     0.866810000000     0.601440000000     5.000000000000        1
        2  H    -0.866810000000     0.601440000000     5.000000000000        2
        3  O     0.000000000000    -0.075790000000     5.000000000000        3
        4 He     0.100000000000    -0.020000000000     7.530000000000        4
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  H     1.638033502034     1.136556880358     9.448630622825        1
        2  H    -1.638033502034     1.136556880358     9.448630622825        2
        3  O     0.000000000000    -0.143222342981     9.448630622825        3
        4 He     0.188972612457    -0.037794522491    14.229637717975        4
     ==============================================================================

  - Cholesky decomposition of AO overlap to get linearly independent AOs:

     Linear dependence threshold:             0.10E-05
     Number of atomic orbitals:               29
     Number of orthonormal atomic orbitals:   29

  - Molecular orbital details:

     Number of occupied orbitals:         6
     Number of virtual orbitals:         23
     Number of molecular orbitals:       29


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a RHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)


  1) Generate initial SAD density


  2) Calculation of reference state (SCF-DIIS algorithm)

  - SCF solver settings:

     Maximum iterations:                   100
     Acceleration type:                   diis

     Residual threshold:            0.1000E-11
     Energy threshold:              0.1000E-11

  - Setting initial AO density to sad

     Energy of initial guess:               -78.492022836315
     Number of electrons in guess:           12.000000000000

  - Screening and integral thresholds:

     Coulomb screening threshold:    0.1000E-17
     Exchange screening threshold:   0.1000E-15
     ERI cutoff:                     0.1000E-17
     One-electron integral  cutoff:  0.1000E-22
     Cumulative Fock threshold:      0.1000E+01

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (solver scf_e): memory
     Storage (solver scf_x): memory

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1           -78.796606592587     0.9053E-01     0.7880E+02
     2           -78.828675852655     0.7128E-01     0.3207E-01
     3           -78.843428154462     0.8290E-02     0.1475E-01
     4           -78.843809541675     0.2414E-02     0.3814E-03
     5           -78.843850846799     0.3301E-03     0.4131E-04
     6           -78.843851669326     0.5527E-04     0.8225E-06
     7           -78.843851692851     0.5865E-05     0.2353E-07
     8           -78.843851693557     0.2356E-05     0.7053E-09
     9           -78.843851693630     0.2071E-06     0.7378E-10
    10           -78.843851693631     0.1594E-07     0.4121E-12
    11           -78.843851693631     0.3322E-08     0.2842E-13
    12           -78.843851693631     0.1197E-08     0.1421E-13
    13           -78.843851693631     0.4264E-09     0.2842E-13
    14           -78.843851693631     0.1280E-09     0.0000E+00
    15           -78.843851693631     0.1479E-10     0.2842E-13
    16           -78.843851693631     0.2905E-11     0.0000E+00
    17           -78.843851693631     0.6422E-12     0.1421E-13
  ---------------------------------------------------------------
  Convergence criterion met in 17 iterations!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.645629080251
     Nuclear repulsion energy:      12.116100574587
     Electronic energy:            -90.959952268218
     Total energy:                 -78.843851693631

  - Timings for the RHF ground state calculation

     Total wall time (sec):              0.34212
     Total cpu time (sec):               0.63707


  :: CCS wavefunction
  ======================

     Bath orbital(s):         False

   - Number of orbitals:

     Occupied orbitals:    6
     Virtual orbitals:     23
     Molecular orbitals:   29
     Atomic orbitals:      29

   - Number of ground state amplitudes:

     Single excitation amplitudes:  138


  :: Time dependent coupled cluster engine
  ===========================================

  Calculates the time dependent CC wavefunction

  This is a CCS time dependent calculation.
  The following tasks will be performed:

     1) Cholesky decomposition of the electron repulsion integrals
     2) Preparation of MO basis and integrals
     3) Calculation of the ground state (diis algorithm)
     4) Calculation of the multipliers (davidson algorithm)
     5) Propagation from the ground state


  :: Time dependent coupled cluster engine
  ===========================================

  Calculates the time dependent CC wavefunction

  This is a CCS time dependent calculation.
  The following tasks will be performed:

     1) Cholesky decomposition of the electron repulsion integrals
     2) Preparation of MO basis and integrals
     3) Calculation of the ground state (diis algorithm)
     4) Calculation of the multipliers (davidson algorithm)
     5) Propagation from the ground state


  1) Cholesky decomposition of the electron repulsion integrals

   - Cholesky decomposition of electronic repulsion integrals solver
  ---------------------------------------------------------------------

  Performs a Cholesky decomposition of the two-electron electronic repulsion 
  integrals in the atomic orbital basis,

  (ab|cd) = sum_J L_ab^J L_cd^J.

  Once the Cholesky basis has been determined, the vectors L^J are constructed 
  and stored to disk. These may either be used directly, or be transformed 
  to the MO basis for use in post-HF calculations. For more information, 
  see S. D. Folkestad, E. F. Kjønstad and H. Koch, JCP, 150(19), (2019)

  - Cholesky decomposition settings:

     Target threshold is:   0.10E-11
     Span factor:           0.10E-01
     Max qual:                  1000

  - Cholesky decomposition ao details:

     Total number of AOs:                    29
     Total number of shell pairs:           120
     Total number of AO pairs:              435

     Significant shell pairs:               118
     Significant AO pairs:                  431

     Construct shell pairs:                 120
     Construct AO pairs:                    435

  Iter.  #Sign. ao pairs / shell pairs   Max diagonal    #Qualified    #Cholesky    Cholesky array size
  -------------------------------------------------------------------------------------------------------
     1               406 /     108       0.47383E+01         147             42             17052
     2               319 /      93       0.47165E-01         234            111             35409
     3               250 /      76       0.46944E-03         178            183             45750
     4               187 /      55       0.38270E-05         145            265             49555
     5                87 /      25       0.38106E-07          78            324             28188
     6                21 /       3       0.37202E-09          43            349              7329
     7                 0 /       0       0.36652E-11           7            354                 0
  -------------------------------------------------------------------------------------------------------

  - Summary of Cholesky decomposition of electronic repulsion integrals:

     Final number of Cholesky vectors: 354

 - Testing the Cholesky decomposition decomposition electronic repulsion integrals:

     Maximal difference between approximate and actual diagonal:              0.7161E-12
     Minimal element of difference between approximate and actual diagonal:  -0.1238E-14

  - Settings for integral handling:

     Cholesky vectors in memory: True
     ERI matrix in memory:       False
     T1 ERI matrix in memory:    False

  - Finished decomposing the ERIs.

     Total wall time (sec):              0.11335
     Total cpu time (sec):               0.22933


  2) Preparation of MO basis and integrals


  3) Calculation of the ground state (diis algorithm)

   - DIIS coupled cluster ground state solver
  ----------------------------------------------

  A DIIS CC ground state amplitude equations solver. It uses an extrapolation 
  of previous quasi-Newton perturbation theory estimates of the next amplitudes. 
  See Helgaker et al., Molecular Electronic Structure Theory, Chapter 
  13.

  - CC ground state solver settings:

     Residual threshold:            0.1000E-09
     Energy threshold:              0.1000E-09
     Max number of iterations:      100

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (cc_gs_diis_e): file
     Storage (cc_gs_diis_x): file

  Iteration    Energy (a.u.)        |omega|       Delta E (a.u.)
  ---------------------------------------------------------------
    1           -78.843851693631     0.1548E-11     0.7884E+02
  ---------------------------------------------------------------
  Convergence criterion met in 1 iterations!

  - Ground state summary:

     Final ground state energy (a.u.):   -78.843851693631

     Correlation energy (a.u.):            0.000000000000

     Largest single amplitudes:
     -----------------------------------
        a       i         t(a,i)
     -----------------------------------
        1      1        0.000000000000
        2      1        0.000000000000
        3      1        0.000000000000
        4      1        0.000000000000
        5      1        0.000000000000
        6      1        0.000000000000
        7      1        0.000000000000
        8      1        0.000000000000
        9      1        0.000000000000
       10      1        0.000000000000
     ------------------------------------

  - Finished solving the CCS ground state equations

     Total wall time (sec):              0.00247
     Total cpu time (sec):               0.00370


  4) Calculation of the multipliers (davidson algorithm)

   - Davidson coupled cluster multipliers solver
  -------------------------------------------------

  A Davidson solver that solves the multiplier equation: t-bar^T A = -η. 
  This linear equation is solved in a reduced space. A description of 
  the algorithm can be found in E. R. Davidson, J. Comput. Phys. 17, 87 
  (1975).

  - Davidson CC multipliers solver settings:

     Residual threshold:        0.10E-09
     Max number of iterations:       100

   - Davidson tool settings:

     Number of parameters:                  138
     Number of requested solutions:           1
     Max reduced space dimension:            50

     Storage (multipliers_trials): file
     Storage (multipliers_transforms): file

  Right hand side is zero to within threshold .1E-09.
  Equations solved with multipliers set to zero!

     Largest single amplitudes:
     -----------------------------------
        a       i         tbar(a,i)
     -----------------------------------
        1      1        0.000000000000
        2      1        0.000000000000
        3      1        0.000000000000
        4      1        0.000000000000
        5      1        0.000000000000
        6      1        0.000000000000
        7      1        0.000000000000
        8      1        0.000000000000
        9      1        0.000000000000
       10      1        0.000000000000
     ------------------------------------

  - Finished solving the CCS multipliers equations

     Total wall time (sec):               0.00030
     Total cpu time (sec):                0.00030

  - Settings for integral handling:

     Cholesky vectors in memory: True
     ERI matrix in memory:       False
     T1 ERI matrix in memory:    False


  5) Propagation from the ground state

   - Runge-Kutta 4 coupled cluster propagation solver
  ------------------------------------------------------

  A solver that propagates a coupled cluster wavefunction from the ground 
  state using the rk4 method.

  - Propagation settings:

     Initial time:     0.0000 au
     Final time:       0.1000 au
     Time step:        0.0200 au
  Properties written at time:     0.0000 au
  Properties written at time:     0.0200 au
  Properties written at time:     0.0400 au
  Properties written at time:     0.0600 au
  Properties written at time:     0.0800 au
  Properties written at time:     0.1000 au

  Finished propagating for     0.1000 au

  - Runge-Kutta 4 coupled cluster propagation solver summary

     Energy after propagation [au]:
     ------------------------------------------
         Real part         Imaginary part
     ------------------------------------------
            44.0972890801     -0.0000000973
     ------------------------------------------

     Dipole moment after propagation [au]:
     ---------------------------------------------
            Real part         Imaginary part
     ---------------------------------------------
         x     -0.0001962652     -0.0000000640
         y      0.8511551089      0.0000002259
         z      0.0203571981      0.0000002562
     ------------------------------------------

  - Timings for the CCS time dependent calculation

     Total wall time (sec):              0.26617
     Total cpu time (sec):               0.53411

  ------------------------------------------------------------

  Peak memory usage during the execution of eT: 14.414752 MB

  Total wall time in eT (sec):              0.62996
  Total cpu time in eT (sec):               1.19880

  Calculation ended: 2021-06-07 12:49:51 UTC +02:00

  - Implementation references:

     eT: https://doi.org/10.1063/5.0004713
     Time-dependent CC: https://doi.org/10.1103/PhysRevA.102.023115
     Cholesky decomposition of ERIs: https://doi.org/10.1063/1.5083802

  eT terminated successfully!




                     eT 1.3 - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, T. Moitra, R. H. Myhre, A. C. Paul, S. Roet, M. Scavino, 
   A. Skeidsvoll, Å. H. Tveten
  ------------------------------------------------------------------------
   J. Chem. Phys. 152, 184103 (2020); https://doi.org/10.1063/5.0004713


   This is eT 1.3.0 Disco
  ------------------------------------------------------------
  Configuration date: 2021-06-07 12:48:33 UTC +02:00
  Git branch:         update-test-refs-new
  Git hash:           07052c810cfb89ed77f5f7b0b9d39be404f0338e
  Fortran compiler:   GNU 10.3.0
  C compiler:         GNU 10.3.0
  C++ compiler:       GNU 10.3.0
  LAPACK type:        MKL
  BLAS type:          MKL
  64-bit integers:    ON
  OpenMP:             ON
  PCM:                ON
  Forced batching:    OFF
  Runtime checks:     OFF
  ------------------------------------------------------------

  Calculation started: 2021-06-07 12:50:26 UTC +02:00


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: H2O He
        charge: 0
        cartesian gaussians
     end system

     do
        ground state
        excited state
     end do

     memory
        available: 8
     end memory

     solver cholesky
        threshold: 1.0d-12
     end solver cholesky

     solver scf
        ao density guess:   core
        algorithm:          scf-diis
        energy threshold:   1.0d-10
        gradient threshold: 1.0d-10
     end solver scf

     method
        hf
        ccsd
     end method

     solver cc gs
        omega threshold:  1.0d-10
        energy threshold: 1.0d-10
     end solver cc gs

     solver cc es
        algorithm:          davidson
        singlet states:     2
        residual threshold: 1.0d-10
        energy threshold:   1.0d-10
        right eigenvectors
     end solver cc es


  Running on 2 OMP threads
  Memory available for calculation: 8.000000 GB


  :: RHF wavefunction
  ======================

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  H     0.866810000000     0.601440000000     5.000000000000        1
        2  H    -0.866810000000     0.601440000000     5.000000000000        2
        3  O     0.000000000000    -0.075790000000     5.000000000000        3
        4 He     0.100000000000    -0.020000000000     7.530000000000        4
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  H     1.638033502034     1.136556880358     9.448630622825        1
        2  H    -1.638033502034     1.136556880358     9.448630622825        2
        3  O     0.000000000000    -0.143222342981     9.448630622825        3
        4 He     0.188972612457    -0.037794522491    14.229637717975        4
     ==============================================================================

  - Cholesky decomposition of AO overlap to get linearly independent AOs:

     Linear dependence threshold:             0.10E-05
     Number of atomic orbitals:               30
     Number of orthonormal atomic orbitals:   30

  - Molecular orbital details:

     Number of occupied orbitals:         6
     Number of virtual orbitals:         24
     Number of molecular orbitals:       30


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a RHF ground state calculation.
  The following tasks will be performed:

     1) Calculation of reference state (SCF-DIIS algorithm)


  1) Calculation of reference state (SCF-DIIS algorithm)

  - SCF solver settings:

     Maximum iterations:                   100
     Acceleration type:                   diis

     Residual threshold:            0.1000E-09
     Energy threshold:              0.1000E-09

  - Setting initial AO density to core

     Energy of initial guess:               -64.220139334736
     Number of electrons in guess:           12.000000000000

  - Screening and integral thresholds:

     Coulomb screening threshold:    0.1000E-15
     Exchange screening threshold:   0.1000E-13
     ERI cutoff:                     0.1000E-15
     One-electron integral  cutoff:  0.1000E-20
     Cumulative Fock threshold:      0.1000E+01

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (solver scf_e): memory
     Storage (solver scf_x): memory

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1           -65.318075774654     0.9968E+00     0.6532E+02
     2           -70.270459480583     0.7596E+00     0.4952E+01
     3           -78.791174523498     0.8490E-01     0.8521E+01
     4           -78.838115632539     0.3257E-01     0.4694E-01
     5           -78.843356467514     0.1002E-01     0.5241E-02
     6           -78.844224402938     0.1191E-02     0.8679E-03
     7           -78.844236930316     0.2796E-03     0.1253E-04
     8           -78.844237827166     0.1642E-03     0.8969E-06
     9           -78.844237988722     0.2666E-04     0.1616E-06
    10           -78.844238004774     0.6067E-05     0.1605E-07
    11           -78.844238005536     0.1820E-05     0.7618E-09
    12           -78.844238005604     0.3058E-06     0.6803E-10
    13           -78.844238005607     0.8266E-07     0.2572E-11
    14           -78.844238005607     0.1656E-07     0.7105E-13
    15           -78.844238005607     0.4960E-08     0.7105E-13
    16           -78.844238005607     0.1565E-08     0.2842E-13
    17           -78.844238005607     0.4019E-09     0.1421E-13
    18           -78.844238005607     0.8055E-10     0.4263E-13
  ---------------------------------------------------------------
  Convergence criterion met in 18 iterations!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.643850177306
     Nuclear repulsion energy:      12.116100574587
     Electronic energy:            -90.960338580194
     Total energy:                 -78.844238005607

  - Timings for the RHF ground state calculation

     Total wall time (sec):              0.21170
     Total cpu time (sec):               0.34717


  :: CCSD wavefunction
  =======================

     Bath orbital(s):         False

   - Number of orbitals:

     Occupied orbitals:    6
     Virtual orbitals:     24
     Molecular orbitals:   30
     Atomic orbitals:      30

   - Number of ground state amplitudes:

     Single excitation amplitudes:  144
     Double excitation amplitudes:  10440


  :: Excited state coupled cluster engine
  ==========================================

  Calculates the coupled cluster excitation vectors and excitation energies

  This is a CCSD excited state calculation.
  The following tasks will be performed:

     1) Cholesky decomposition of the electron repulsion integrals
     2) Preparation of MO basis and integrals
     3) Calculation of the ground state (diis algorithm)
     4) Calculation of the excited state (davidson algorithm)


  1) Cholesky decomposition of the electron repulsion integrals

   - Cholesky decomposition of electronic repulsion integrals solver
  ---------------------------------------------------------------------

  Performs a Cholesky decomposition of the two-electron electronic repulsion 
  integrals in the atomic orbital basis,

  (ab|cd) = sum_J L_ab^J L_cd^J.

  Once the Cholesky basis has been determined, the vectors L^J are constructed 
  and stored to disk. These may either be used directly, or be transformed 
  to the MO basis for use in post-HF calculations. For more information, 
  see S. D. Folkestad, E. F. Kjønstad and H. Koch, JCP, 150(19), (2019)

  - Cholesky decomposition settings:

     Target threshold is:   0.10E-11
     Span factor:           0.10E-01
     Max qual:                  1000

  - Cholesky decomposition ao details:

     Total number of AOs:                    30
     Total number of shell pairs:           120
     Total number of AO pairs:              465

     Significant shell pairs:               118
     Significant AO pairs:                  461

     Construct shell pairs:                 120
     Construct AO pairs:                    465

  Iter.  #Sign. ao pairs / shell pairs   Max diagonal    #Qualified    #Cholesky    Cholesky array size
  -------------------------------------------------------------------------------------------------------
     1               433 /     108       0.47383E+01         143             41             17753
     2               360 /      95       0.46181E-01         259            103             37080
     3               297 /      78       0.40867E-03         218            183             54351
     4               197 /      56       0.40673E-05         155            264             52008
     5                92 /      26       0.39075E-07          90            329             30268
     6                20 /       4       0.29139E-09          48            358              7160
     7                 0 /       0       0.27816E-11           6            362                 0
  -------------------------------------------------------------------------------------------------------

  - Summary of Cholesky decomposition of electronic repulsion integrals:

     Final number of Cholesky vectors: 362

 - Testing the Cholesky decomposition decomposition electronic repulsion integrals:

     Maximal difference between approximate and actual diagonal:              0.9596E-12
     Minimal element of difference between approximate and actual diagonal:  -0.1238E-14

  - Settings for integral handling:

     Cholesky vectors in memory: True
     ERI matrix in memory:       False
     T1 ERI matrix in memory:    True

  - Finished decomposing the ERIs.

     Total wall time (sec):              0.12423
     Total cpu time (sec):               0.24748


  2) Preparation of MO basis and integrals


  3) Calculation of the ground state (diis algorithm)

   - DIIS coupled cluster ground state solver
  ----------------------------------------------

  A DIIS CC ground state amplitude equations solver. It uses an extrapolation 
  of previous quasi-Newton perturbation theory estimates of the next amplitudes. 
  See Helgaker et al., Molecular Electronic Structure Theory, Chapter 
  13.

  - CC ground state solver settings:

     Residual threshold:            0.1000E-09
     Energy threshold:              0.1000E-09
     Max number of iterations:      100

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (cc_gs_diis_e): file
     Storage (cc_gs_diis_x): file

  Iteration    Energy (a.u.)        |omega|       Delta E (a.u.)
  ---------------------------------------------------------------
    1           -79.088038345305     0.9716E-01     0.7909E+02
    2           -79.095902957684     0.2750E-01     0.7865E-02
    3           -79.102836104432     0.7664E-02     0.6933E-02
    4           -79.103797373086     0.2121E-02     0.9613E-03
    5           -79.103818257501     0.5267E-03     0.2088E-04
    6           -79.103840887355     0.2407E-03     0.2263E-04
    7           -79.103832104161     0.5120E-04     0.8783E-05
    8           -79.103830694309     0.1229E-04     0.1410E-05
    9           -79.103830087331     0.4227E-05     0.6070E-06
   10           -79.103829881919     0.1793E-05     0.2054E-06
   11           -79.103829919472     0.6986E-06     0.3755E-07
   12           -79.103829929254     0.3166E-06     0.9782E-08
   13           -79.103829942686     0.1089E-06     0.1343E-07
   14           -79.103829937354     0.3291E-07     0.5332E-08
   15           -79.103829936328     0.7089E-08     0.1026E-08
   16           -79.103829936570     0.2543E-08     0.2415E-09
   17           -79.103829936847     0.7701E-09     0.2769E-09
   18           -79.103829936926     0.1813E-09     0.7959E-10
   19           -79.103829936936     0.6019E-10     0.9592E-11
  ---------------------------------------------------------------
  Convergence criterion met in 19 iterations!

  - Ground state summary:

     Final ground state energy (a.u.):   -79.103829936936

     Correlation energy (a.u.):           -0.259591931329

     Largest single amplitudes:
     -----------------------------------
        a       i         t(a,i)
     -----------------------------------
        1      5        0.015138391220
       14      4       -0.009537658253
        7      4        0.008298169066
       15      5       -0.006048036436
        4      5        0.005680074429
       12      5        0.005571453417
        6      2        0.005315692248
        2      4        0.005170387080
        5      6        0.004900823132
       11      6       -0.003394001676
     ------------------------------------

     Largest double amplitudes:
     --------------------------------------------------
        a      i       b      j         t(ai,bj)
     --------------------------------------------------
        2      4       2      4       -0.047345850155
        5      6       5      6       -0.045278496561
        3      4       3      4       -0.036620604731
       10      3      10      3       -0.035692307307
        6      5       6      5       -0.034561514693
        1      5       1      5       -0.034427925105
       16      3      16      3       -0.032108049370
       17      3      17      3       -0.032052423962
       18      3      18      3       -0.031350315536
        2      4       3      4       -0.029687812128
     --------------------------------------------------

  - Finished solving the CCSD ground state equations

     Total wall time (sec):              0.16745
     Total cpu time (sec):               0.33502


  4) Calculation of the excited state (davidson algorithm)
     Calculating right vectors

   - Davidson coupled cluster excited state solver
  ---------------------------------------------------

  A Davidson solver that calculates the lowest eigenvalues and the right 
  or left eigenvectors of the Jacobian matrix, A. The eigenvalue problem 
  is solved in a reduced space, the dimension of which is expanded until 
  the convergence criteria are met.

  A complete description of the algorithm can be found in E. R. Davidson, 
  J. Comput. Phys. 17, 87 (1975).

  - Settings for coupled cluster excited state solver (Davidson):

     Calculation type:    valence
     Excitation vectors:  right

     Residual threshold:            0.1000E-09
     Energy threshold:              0.1000E-09

     Number of singlet states:               2
     Max number of iterations:             100

   - Davidson tool settings:

     Number of parameters:                10584
     Number of requested solutions:           2
     Max reduced space dimension:           100

     Storage (cc_es_davidson_trials): file
     Storage (cc_es_davidson_transforms): file

  Iteration:                  1
  Reduced space dimension:    2

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.377855227812    0.000000000000     0.4458E+00   0.3779E+00
     2   0.486072650143    0.000000000000     0.4306E+00   0.4861E+00
  ------------------------------------------------------------------------

  Iteration:                  2
  Reduced space dimension:    4

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.246851644204    0.000000000000     0.9240E-01   0.1310E+00
     2   0.361540980045    0.000000000000     0.1131E+00   0.1245E+00
  ------------------------------------------------------------------------

  Iteration:                  3
  Reduced space dimension:    6

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.245650617083    0.000000000000     0.3336E-01   0.1201E-02
     2   0.355231109261    0.000000000000     0.4359E-01   0.6310E-02
  ------------------------------------------------------------------------

  Iteration:                  4
  Reduced space dimension:    8

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.246340721065    0.000000000000     0.9798E-02   0.6901E-03
     2   0.356303773151    0.000000000000     0.1498E-01   0.1073E-02
  ------------------------------------------------------------------------

  Iteration:                  5
  Reduced space dimension:   10

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.246354891507    0.000000000000     0.2063E-02   0.1417E-04
     2   0.355854330840    0.000000000000     0.4045E-02   0.4494E-03
  ------------------------------------------------------------------------

  Iteration:                  6
  Reduced space dimension:   12

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.246306984275    0.000000000000     0.5617E-03   0.4791E-04
     2   0.355822924244    0.000000000000     0.1646E-02   0.3141E-04
  ------------------------------------------------------------------------

  Iteration:                  7
  Reduced space dimension:   14

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.246305458151    0.000000000000     0.1681E-03   0.1526E-05
     2   0.355810037334    0.000000000000     0.6035E-03   0.1289E-04
  ------------------------------------------------------------------------

  Iteration:                  8
  Reduced space dimension:   16

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.246305943310    0.000000000000     0.5953E-04   0.4852E-06
     2   0.355822380881    0.000000000000     0.2551E-03   0.1234E-04
  ------------------------------------------------------------------------

  Iteration:                  9
  Reduced space dimension:   18

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.246306026089    0.000000000000     0.3622E-04   0.8278E-07
     2   0.355808599811    0.000000000000     0.4698E-03   0.1378E-04
  ------------------------------------------------------------------------

  Iteration:                 10
  Reduced space dimension:   20

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.246306079718    0.000000000000     0.2556E-04   0.5363E-07
     2   0.328681310938    0.000000000000     0.1759E+00   0.2713E-01
  ------------------------------------------------------------------------

  Iteration:                 11
  Reduced space dimension:   22

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.246306094133    0.000000000000     0.8962E-05   0.1441E-07
     2   0.313915218690    0.000000000000     0.5426E-01   0.1477E-01
  ------------------------------------------------------------------------

  Iteration:                 12
  Reduced space dimension:   24

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.246306087189    0.000000000000     0.2797E-05   0.6943E-08
     2   0.312787038270    0.000000000000     0.1605E-01   0.1128E-02
  ------------------------------------------------------------------------

  Iteration:                 13
  Reduced space dimension:   26

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.246306077334    0.000000000000     0.1013E-05   0.9855E-08
     2   0.313019436662    0.000000000000     0.6736E-02   0.2324E-03
  ------------------------------------------------------------------------

  Iteration:                 14
  Reduced space dimension:   28

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.246306073726    0.000000000000     0.5189E-06   0.3609E-08
     2   0.312919456436    0.000000000000     0.4649E-02   0.9998E-04
  ------------------------------------------------------------------------

  Iteration:                 15
  Reduced space dimension:   30

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.246306074183    0.000000000000     0.2314E-06   0.4568E-09
     2   0.312871351193    0.000000000000     0.2521E-02   0.4811E-04
  ------------------------------------------------------------------------

  Iteration:                 16
  Reduced space dimension:   32

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.246306074260    0.000000000000     0.9636E-07   0.7685E-10
     2   0.312853431729    0.000000000000     0.1162E-02   0.1792E-04
  ------------------------------------------------------------------------

  Iteration:                 17
  Reduced space dimension:   34

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.246306074162    0.000000000000     0.3630E-07   0.9749E-10
     2   0.312852468915    0.000000000000     0.5133E-03   0.9628E-06
  ------------------------------------------------------------------------

  Iteration:                 18
  Reduced space dimension:   36

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.246306074250    0.000000000000     0.1085E-07   0.8772E-10
     2   0.312857384059    0.000000000000     0.1864E-03   0.4915E-05
  ------------------------------------------------------------------------

  Iteration:                 19
  Reduced space dimension:   38

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.246306074226    0.000000000000     0.3163E-08   0.2360E-10
     2   0.312855560826    0.000000000000     0.6160E-04   0.1823E-05
  ------------------------------------------------------------------------

  Iteration:                 20
  Reduced space dimension:   40

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.246306074214    0.000000000000     0.1019E-08   0.1240E-10
     2   0.312855689496    0.000000000000     0.2031E-04   0.1287E-06
  ------------------------------------------------------------------------

  Iteration:                 21
  Reduced space dimension:   42

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.246306074213    0.000000000000     0.2833E-09   0.1326E-11
     2   0.312855765051    0.000000000000     0.5973E-05   0.7556E-07
  ------------------------------------------------------------------------

  Iteration:                 22
  Reduced space dimension:   44

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.246306074213    0.000000000000     0.7549E-10   0.9054E-12
     2   0.312855725742    0.000000000000     0.1649E-05   0.3931E-07
  ------------------------------------------------------------------------

  Iteration:                 23
  Reduced space dimension:   45

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.246306074214    0.000000000000     0.2745E-10   0.5322E-12
     2   0.312855726453    0.000000000000     0.4599E-06   0.7108E-09
  ------------------------------------------------------------------------

  Iteration:                 24
  Reduced space dimension:   46

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.246306074214    0.000000000000     0.1891E-10   0.1209E-12
     2   0.312855725165    0.000000000000     0.1336E-06   0.1288E-08
  ------------------------------------------------------------------------

  Iteration:                 25
  Reduced space dimension:   47

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.246306074214    0.000000000000     0.1850E-10   0.3136E-14
     2   0.312855725357    0.000000000000     0.3984E-07   0.1916E-09
  ------------------------------------------------------------------------

  Iteration:                 26
  Reduced space dimension:   48

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.246306074214    0.000000000000     0.1838E-10   0.2220E-15
     2   0.312855725738    0.000000000000     0.1112E-07   0.3811E-09
  ------------------------------------------------------------------------

  Iteration:                 27
  Reduced space dimension:   49

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.246306074214    0.000000000000     0.1831E-10   0.2554E-14
     2   0.312855725642    0.000000000000     0.3351E-08   0.9605E-10
  ------------------------------------------------------------------------

  Iteration:                 28
  Reduced space dimension:   50

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.246306074214    0.000000000000     0.1830E-10   0.4996E-15
     2   0.312855725622    0.000000000000     0.1077E-08   0.1938E-10
  ------------------------------------------------------------------------

  Iteration:                 29
  Reduced space dimension:   51

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.246306074214    0.000000000000     0.1827E-10   0.9159E-14
     2   0.312855725629    0.000000000000     0.3377E-09   0.6858E-11
  ------------------------------------------------------------------------

  Iteration:                 30
  Reduced space dimension:   52

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.246306074214    0.000000000000     0.1826E-10   0.1305E-14
     2   0.312855725628    0.000000000000     0.9102E-10   0.1567E-11
  ------------------------------------------------------------------------
  Convergence criterion met in 30 iterations!

  - Excitation vector amplitudes:

     Electronic state nr. 1

     Energy (Hartree):                  0.246306074214
     Fraction singles (|R1|/|R|):       0.973361941282

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        1      6        0.964028194983
        4      6        0.122740765432
        6      6       -0.034157906142
       12      6       -0.029736575845
        8      6        0.018116898234
        1      3       -0.011050788386
       11      6       -0.009369927530
       22      6        0.008849913186
        1      5        0.007996938936
       19      5       -0.007872108958
     ------------------------------------

     Largest double amplitudes:
     --------------------------------------------------
        a      i       b      j         R(ai,bj)
     --------------------------------------------------
        2      4       1      6       -0.100871235682
        1      5       1      6       -0.086785991373
        1      6       5      6       -0.065885562814
        1      2       1      6       -0.062716061609
        1      4       2      6       -0.061450112564
        3      4       1      6       -0.054787248113
        6      5       1      6        0.046182835536
        4      5       1      6       -0.038270831996
        7      4       1      6        0.032951248928
        4      4       2      6       -0.032775080205
     --------------------------------------------------

     Electronic state nr. 2

     Energy (Hartree):                  0.312855725628
     Fraction singles (|R1|/|R|):       0.974127455409

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        2      6       -0.954502464252
        3      6       -0.178575866898
        7      6        0.072421447722
       14      6       -0.020797770971
        2      3        0.011051087836
       13      6        0.005861005272
        9      2        0.005703595497
        2      5       -0.005247127315
        5      4        0.004848559443
        3      3        0.003611854465
     ------------------------------------

     Largest double amplitudes:
     --------------------------------------------------
        a      i       b      j         R(ai,bj)
     --------------------------------------------------
        2      4       2      6        0.110576889474
        1      5       2      6        0.080011742207
        2      6       5      6        0.059707467605
        2      5       1      6        0.050415231072
        3      4       2      6        0.047545587392
        1      4       1      6        0.045328147689
        1      2       2      6        0.044650335753
        4      5       2      6        0.039713879743
        6      5       2      6       -0.038891666180
        3      5       1      6        0.034270279041
     --------------------------------------------------

     - Electronic excitation energies:

                                      Excitation energy
                          ------------------------------------------
      State                (Hartree)             (eV)
     ---------------------------------------------------------------
        1                  0.246306074214        6.702329664511
        2                  0.312855725628        8.513237918620
     ---------------------------------------------------------------
     eV/Hartree (CODATA 2014): 27.21138602

  - Finished solving the CCSD excited state equations (right)

     Total wall time (sec):              0.30012
     Total cpu time (sec):               0.59781

  - Timings for the CCSD excited state calculation

     Total wall time (sec):              0.59224
     Total cpu time (sec):               1.18076

  ------------------------------------------------------------

  Peak memory usage during the execution of eT: 18.127144 MB

  Total wall time in eT (sec):              0.82560
  Total cpu time in eT (sec):               1.55524

  Calculation ended: 2021-06-07 12:50:27 UTC +02:00

  - Implementation references:

     eT: https://doi.org/10.1063/5.0004713
     Cholesky decomposition of ERIs: https://doi.org/10.1063/1.5083802

  eT terminated successfully!




                     eT 1.3 - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, T. Moitra, R. H. Myhre, A. C. Paul, S. Roet, M. Scavino, 
   A. Skeidsvoll, Å. H. Tveten
  ------------------------------------------------------------------------
   J. Chem. Phys. 152, 184103 (2020); https://doi.org/10.1063/5.0004713


   This is eT 1.3.0 Disco
  ------------------------------------------------------------
  Configuration date: 2021-06-07 12:48:33 UTC +02:00
  Git branch:         update-test-refs-new
  Git hash:           07052c810cfb89ed77f5f7b0b9d39be404f0338e
  Fortran compiler:   GNU 10.3.0
  C compiler:         GNU 10.3.0
  C++ compiler:       GNU 10.3.0
  LAPACK type:        MKL
  BLAS type:          MKL
  64-bit integers:    ON
  OpenMP:             ON
  PCM:                ON
  Forced batching:    OFF
  Runtime checks:     OFF
  ------------------------------------------------------------

  Calculation started: 2021-06-07 12:49:34 UTC +02:00


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: OH-
        charge: -1
        multiplicity: 1
     end system

     do
        ground state
     end do

     method
        hf
     end method

     memory
        available: 8
     end memory

     solver scf
       algorithm:          scf-diis
       energy threshold:   1.0d-10
       gradient threshold: 1.0d-10
     end solver scf


  Running on 2 OMP threads
  Memory available for calculation: 8.000000 GB


  :: RHF wavefunction
  ======================

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: 3-21g
        1  O     0.000000000000     0.000000000000     0.000000000000        1
        2  H     0.000000000000     0.000000000000     0.800000000000        2
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: 3-21g
        1  O     0.000000000000     0.000000000000     0.000000000000        1
        2  H     0.000000000000     0.000000000000     1.511780899652        2
     ==============================================================================

  - Cholesky decomposition of AO overlap to get linearly independent AOs:

     Linear dependence threshold:             0.10E-05
     Number of atomic orbitals:               11
     Number of orthonormal atomic orbitals:   11

  - Molecular orbital details:

     Number of occupied orbitals:         5
     Number of virtual orbitals:          6
     Number of molecular orbitals:       11


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a RHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)


  1) Generate initial SAD density


  2) Calculation of reference state (SCF-DIIS algorithm)

  - SCF solver settings:

     Maximum iterations:                   100
     Acceleration type:                   diis

     Residual threshold:            0.1000E-09
     Energy threshold:              0.1000E-09

  - Setting initial AO density to sad

     Energy of initial guess:               -74.800893188501
     Number of electrons in guess:            9.000000000000
     Overall charge:                                      -1

  - Screening and integral thresholds:

     Coulomb screening threshold:    0.1000E-15
     Exchange screening threshold:   0.1000E-13
     ERI cutoff:                     0.1000E-15
     One-electron integral  cutoff:  0.1000E-20
     Cumulative Fock threshold:      0.1000E+01

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (solver scf_e): memory
     Storage (solver scf_x): memory

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1           -74.716243720030     0.1704E+00     0.7472E+02
     2           -74.797707312304     0.8026E-01     0.8146E-01
     3           -74.815146759874     0.5068E-02     0.1744E-01
     4           -74.815302173361     0.1119E-02     0.1554E-03
     5           -74.815315529766     0.6550E-04     0.1336E-04
     6           -74.815315619317     0.1117E-04     0.8955E-07
     7           -74.815315619821     0.5939E-06     0.5044E-09
     8           -74.815315619823     0.4354E-07     0.2217E-11
     9           -74.815315619823     0.3154E-08     0.4263E-13
    10           -74.815315619823     0.2328E-09     0.1421E-13
    11           -74.815315619823     0.1622E-10     0.2842E-13
  ---------------------------------------------------------------
  Convergence criterion met in 11 iterations!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.648960723273
     Nuclear repulsion energy:       5.291772109200
     Electronic energy:            -80.107087729023
     Total energy:                 -74.815315619823

  - Timings for the RHF ground state calculation

     Total wall time (sec):              0.05569
     Total cpu time (sec):               0.04052

  ------------------------------------------------------------

  Peak memory usage during the execution of eT: 26.560 KB

  Total wall time in eT (sec):              0.06047
  Total cpu time in eT (sec):               0.04530

  Calculation ended: 2021-06-07 12:49:35 UTC +02:00

  - Implementation references:

     eT: https://doi.org/10.1063/5.0004713

  eT terminated successfully!

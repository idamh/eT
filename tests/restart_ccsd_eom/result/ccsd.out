


                     eT 1.3 - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, T. Moitra, R. H. Myhre, A. C. Paul, S. Roet, M. Scavino, 
   A. Skeidsvoll, Å. H. Tveten
  ------------------------------------------------------------------------
   J. Chem. Phys. 152, 184103 (2020); https://doi.org/10.1063/5.0004713


   This is eT 1.3.0 Disco
  ------------------------------------------------------------
  Configuration date: 2021-06-07 12:48:33 UTC +02:00
  Git branch:         update-test-refs-new
  Git hash:           07052c810cfb89ed77f5f7b0b9d39be404f0338e
  Fortran compiler:   GNU 10.3.0
  C compiler:         GNU 10.3.0
  C++ compiler:       GNU 10.3.0
  LAPACK type:        MKL
  BLAS type:          MKL
  64-bit integers:    ON
  OpenMP:             ON
  PCM:                ON
  Forced batching:    OFF
  Runtime checks:     OFF
  ------------------------------------------------------------

  Calculation started: 2021-06-07 12:50:55 UTC +02:00


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: H2O He
        charge: 0
     end system

     do
       response
     end do

     memory
        available: 8
     end memory

     solver cholesky
        threshold: 1.0d-12
     end solver cholesky

     solver scf
        energy threshold:   1.0d-11
        gradient threshold: 1.0d-11
     end solver scf

     method
        hf
        ccsd
     end method

     solver cc gs
        omega threshold:  1.0d-11
        energy threshold: 1.0d-11
     end solver cc gs

     solver cc es
        algorithm:          davidson
        singlet states:     3
        residual threshold: 1.0d-11
        energy threshold:   1.0d-11
     end solver cc es

     cc response
       eom
       dipole length
       transition moments
     end cc response

     solver cc multipliers
        threshold:         1.0d-12
        algorithm:         davidson
        max iterations:    5
     end solver cc multipliers


  Running on 2 OMP threads
  Memory available for calculation: 8.000000 GB


  :: RHF wavefunction
  ======================

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  H     0.866810000000     0.601440000000     5.000000000000        1
        2  H    -0.866810000000     0.601440000000     5.000000000000        2
        3  O     0.000000000000    -0.075790000000     5.000000000000        3
        4 He     0.100000000000    -0.020000000000     7.530000000000        4
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  H     1.638033502034     1.136556880358     9.448630622825        1
        2  H    -1.638033502034     1.136556880358     9.448630622825        2
        3  O     0.000000000000    -0.143222342981     9.448630622825        3
        4 He     0.188972612457    -0.037794522491    14.229637717975        4
     ==============================================================================

  - Cholesky decomposition of AO overlap to get linearly independent AOs:

     Linear dependence threshold:             0.10E-05
     Number of atomic orbitals:               29
     Number of orthonormal atomic orbitals:   29

  - Molecular orbital details:

     Number of occupied orbitals:         6
     Number of virtual orbitals:         23
     Number of molecular orbitals:       29


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a RHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)


  1) Generate initial SAD density


  2) Calculation of reference state (SCF-DIIS algorithm)

  - SCF solver settings:

     Maximum iterations:                   100
     Acceleration type:                   diis

     Residual threshold:            0.1000E-10
     Energy threshold:              0.1000E-10

  - Setting initial AO density to sad

     Energy of initial guess:               -78.492022836321
     Number of electrons in guess:           12.000000000000

  - Screening and integral thresholds:

     Coulomb screening threshold:    0.1000E-16
     Exchange screening threshold:   0.1000E-14
     ERI cutoff:                     0.1000E-16
     One-electron integral  cutoff:  0.1000E-21
     Cumulative Fock threshold:      0.1000E+01

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (solver scf_e): memory
     Storage (solver scf_x): memory

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1           -78.796606592593     0.9053E-01     0.7880E+02
     2           -78.828675852657     0.7128E-01     0.3207E-01
     3           -78.843428154462     0.8290E-02     0.1475E-01
     4           -78.843809541675     0.2414E-02     0.3814E-03
     5           -78.843850846799     0.3301E-03     0.4131E-04
     6           -78.843851669326     0.5527E-04     0.8225E-06
     7           -78.843851692851     0.5865E-05     0.2353E-07
     8           -78.843851693557     0.2356E-05     0.7054E-09
     9           -78.843851693630     0.2071E-06     0.7375E-10
    10           -78.843851693631     0.1594E-07     0.4832E-12
    11           -78.843851693631     0.3322E-08     0.5684E-13
    12           -78.843851693631     0.1197E-08     0.1421E-13
    13           -78.843851693631     0.4264E-09     0.0000E+00
    14           -78.843851693631     0.1280E-09     0.4263E-13
    15           -78.843851693631     0.1479E-10     0.0000E+00
    16           -78.843851693631     0.2908E-11     0.4263E-13
  ---------------------------------------------------------------
  Convergence criterion met in 16 iterations!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.645629080248
     Nuclear repulsion energy:      12.116100574587
     Electronic energy:            -90.959952268218
     Total energy:                 -78.843851693631

  - Timings for the RHF ground state calculation

     Total wall time (sec):              0.30314
     Total cpu time (sec):               0.55936


  :: CCSD wavefunction
  =======================

     Bath orbital(s):         False

   - Number of orbitals:

     Occupied orbitals:    6
     Virtual orbitals:     23
     Molecular orbitals:   29
     Atomic orbitals:      29

   - Number of ground state amplitudes:

     Single excitation amplitudes:  138
     Double excitation amplitudes:  9591


  :: Coupled cluster response engine
  =====================================

  Calculates dipole transition moments and oscillator strengths between 

  the ground state and the excited states.

  This is a CCSD response calculation.
  The following tasks will be performed:

     1) Cholesky decomposition of the electron repulsion integrals
     2) Preparation of MO basis and integrals
     3) Calculation of the ground state (diis algorithm)
     4) Calculation of the multipliers (davidson algorithm)
     5) Calculation of the excited states (davidson algorithm)
     6) Calculation of excited state properties (EOM)


  1) Cholesky decomposition of the electron repulsion integrals

   - Cholesky decomposition of electronic repulsion integrals solver
  ---------------------------------------------------------------------

  Performs a Cholesky decomposition of the two-electron electronic repulsion 
  integrals in the atomic orbital basis,

  (ab|cd) = sum_J L_ab^J L_cd^J.

  Once the Cholesky basis has been determined, the vectors L^J are constructed 
  and stored to disk. These may either be used directly, or be transformed 
  to the MO basis for use in post-HF calculations. For more information, 
  see S. D. Folkestad, E. F. Kjønstad and H. Koch, JCP, 150(19), (2019)

  - Cholesky decomposition settings:

     Target threshold is:   0.10E-11
     Span factor:           0.10E-01
     Max qual:                  1000

  - Cholesky decomposition ao details:

     Total number of AOs:                    29
     Total number of shell pairs:           120
     Total number of AO pairs:              435

     Significant shell pairs:               118
     Significant AO pairs:                  431

     Construct shell pairs:                 120
     Construct AO pairs:                    435

  Iter.  #Sign. ao pairs / shell pairs   Max diagonal    #Qualified    #Cholesky    Cholesky array size
  -------------------------------------------------------------------------------------------------------
     1               406 /     108       0.47383E+01         147             42             17052
     2               319 /      93       0.47165E-01         234            111             35409
     3               250 /      76       0.46944E-03         178            183             45750
     4               187 /      55       0.38270E-05         145            265             49555
     5                87 /      25       0.38106E-07          78            324             28188
     6                21 /       3       0.37202E-09          43            349              7329
     7                 0 /       0       0.36652E-11           7            354                 0
  -------------------------------------------------------------------------------------------------------

  - Summary of Cholesky decomposition of electronic repulsion integrals:

     Final number of Cholesky vectors: 354

 - Testing the Cholesky decomposition decomposition electronic repulsion integrals:

     Maximal difference between approximate and actual diagonal:              0.7161E-12
     Minimal element of difference between approximate and actual diagonal:  -0.1238E-14

  - Settings for integral handling:

     Cholesky vectors in memory: True
     ERI matrix in memory:       False
     T1 ERI matrix in memory:    True

  - Finished decomposing the ERIs.

     Total wall time (sec):              0.11585
     Total cpu time (sec):               0.23145


  2) Preparation of MO basis and integrals


  3) Calculation of the ground state (diis algorithm)

   - DIIS coupled cluster ground state solver
  ----------------------------------------------

  A DIIS CC ground state amplitude equations solver. It uses an extrapolation 
  of previous quasi-Newton perturbation theory estimates of the next amplitudes. 
  See Helgaker et al., Molecular Electronic Structure Theory, Chapter 
  13.

  - CC ground state solver settings:

     Residual threshold:            0.1000E-10
     Energy threshold:              0.1000E-10
     Max number of iterations:      100

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (cc_gs_diis_e): file
     Storage (cc_gs_diis_x): file

  Iteration    Energy (a.u.)        |omega|       Delta E (a.u.)
  ---------------------------------------------------------------
    1           -79.084241931608     0.9391E-01     0.7908E+02
    2           -79.092586666607     0.2720E-01     0.8345E-02
    3           -79.099408028306     0.7507E-02     0.6821E-02
    4           -79.100345871309     0.2095E-02     0.9378E-03
    5           -79.100371860301     0.5154E-03     0.2599E-04
    6           -79.100393801016     0.2313E-03     0.2194E-04
    7           -79.100385611260     0.4933E-04     0.8190E-05
    8           -79.100384217524     0.1180E-04     0.1394E-05
    9           -79.100383621794     0.4135E-05     0.5957E-06
   10           -79.100383427012     0.1779E-05     0.1948E-06
   11           -79.100383466393     0.6705E-06     0.3938E-07
   12           -79.100383474630     0.2989E-06     0.8237E-08
   13           -79.100383487351     0.1028E-06     0.1272E-07
   14           -79.100383481865     0.3056E-07     0.5487E-08
   15           -79.100383481092     0.6508E-08     0.7730E-09
   16           -79.100383481302     0.2112E-08     0.2109E-09
   17           -79.100383481484     0.6588E-09     0.1819E-09
   18           -79.100383481546     0.1594E-09     0.6158E-10
   19           -79.100383481554     0.5326E-10     0.7901E-11
   20           -79.100383481555     0.2008E-10     0.1208E-11
   21           -79.100383481556     0.7516E-11     0.6679E-12
  ---------------------------------------------------------------
  Convergence criterion met in 21 iterations!

  - Ground state summary:

     Final ground state energy (a.u.):   -79.100383481556

     Correlation energy (a.u.):           -0.256531787925

     Largest single amplitudes:
     -----------------------------------
        a       i         t(a,i)
     -----------------------------------
        1      5        0.014740597524
       14      4       -0.009546856220
        7      4        0.008284826483
       15      5       -0.006124828873
        4      5        0.005606072698
        6      2        0.005476844297
        2      4        0.005318591701
       13      5        0.005269818337
        5      6        0.004933006906
       11      6       -0.003454309399
     ------------------------------------

     Largest double amplitudes:
     --------------------------------------------------
        a      i       b      j         t(ai,bj)
     --------------------------------------------------
        2      4       2      4       -0.047351708919
        5      6       5      6       -0.046240574404
        9      3       9      3       -0.041367012248
        3      4       3      4       -0.036659067517
        6      5       6      5       -0.034554012170
        1      5       1      5       -0.034177347751
       16      3      16      3       -0.032108235347
       17      3      17      3       -0.032052553603
       18      3      18      3       -0.031351828684
        2      4       3      4       -0.029701270698
     --------------------------------------------------

  - Finished solving the CCSD ground state equations

     Total wall time (sec):              0.18539
     Total cpu time (sec):               0.36845


  4) Calculation of the multipliers (davidson algorithm)

   - Davidson coupled cluster multipliers solver
  -------------------------------------------------

  A Davidson solver that solves the multiplier equation: t-bar^T A = -η. 
  This linear equation is solved in a reduced space. A description of 
  the algorithm can be found in E. R. Davidson, J. Comput. Phys. 17, 87 
  (1975).

  - Davidson CC multipliers solver settings:

     Residual threshold:        0.10E-11
     Max number of iterations:         5

   - Davidson tool settings:

     Number of parameters:                 9729
     Number of requested solutions:           1
     Max reduced space dimension:            50

     Storage (multipliers_trials): file
     Storage (multipliers_transforms): file

  Iteration     Residual norm
  ---------------------------
    1             0.3520E-01
    2             0.6901E-02
    3             0.1186E-02
    4             0.6190E-03
    5             0.1749E-03
    6             0.4640E-04
  ---------------------------

  Error: was not able to converge the equations in the given number of 
         iterations in run_davidson_cc_multipliers.

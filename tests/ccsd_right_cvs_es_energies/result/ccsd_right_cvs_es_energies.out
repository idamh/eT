


                     eT 1.3 - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, T. Moitra, R. H. Myhre, A. C. Paul, S. Roet, M. Scavino, 
   A. Skeidsvoll, Å. H. Tveten
  ------------------------------------------------------------------------
   J. Chem. Phys. 152, 184103 (2020); https://doi.org/10.1063/5.0004713


   This is eT 1.3.0 Disco
  ------------------------------------------------------------
  Configuration date: 2021-06-07 12:48:33 UTC +02:00
  Git branch:         update-test-refs-new
  Git hash:           07052c810cfb89ed77f5f7b0b9d39be404f0338e
  Fortran compiler:   GNU 10.3.0
  C compiler:         GNU 10.3.0
  C++ compiler:       GNU 10.3.0
  LAPACK type:        MKL
  BLAS type:          MKL
  64-bit integers:    ON
  OpenMP:             ON
  PCM:                ON
  Forced batching:    OFF
  Runtime checks:     OFF
  ------------------------------------------------------------

  Calculation started: 2021-06-07 12:50:25 UTC +02:00


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: H2O He
        charge: 0
     end system

     do
        ground state
        excited state
     end do

     memory
        available: 8
     end memory

     solver cholesky
        threshold: 1.0d-12
     end solver cholesky

     solver scf
        algorithm:          scf-diis
        energy threshold:   1.0d-10
        gradient threshold: 1.0d-10
     end solver scf

     method
        hf
        ccsd
     end method

     solver cc gs
        omega threshold:  1.0d-10
        energy threshold: 1.0d-10
     end solver cc gs

     solver cc es
        core excitation:    {1}
        algorithm:          davidson
        singlet states:     2
        residual threshold: 1.0d-10
        energy threshold:   1.0d-10
        right eigenvectors
     end solver cc es


  Running on 2 OMP threads
  Memory available for calculation: 8.000000 GB


  :: RHF wavefunction
  ======================

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  H     0.866810000000     0.601440000000     5.000000000000        1
        2  H    -0.866810000000     0.601440000000     5.000000000000        2
        3  O     0.000000000000    -0.075790000000     5.000000000000        3
        4 He     0.100000000000    -0.020000000000     7.530000000000        4
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  H     1.638033502034     1.136556880358     9.448630622825        1
        2  H    -1.638033502034     1.136556880358     9.448630622825        2
        3  O     0.000000000000    -0.143222342981     9.448630622825        3
        4 He     0.188972612457    -0.037794522491    14.229637717975        4
     ==============================================================================

  - Cholesky decomposition of AO overlap to get linearly independent AOs:

     Linear dependence threshold:             0.10E-05
     Number of atomic orbitals:               29
     Number of orthonormal atomic orbitals:   29

  - Molecular orbital details:

     Number of occupied orbitals:         6
     Number of virtual orbitals:         23
     Number of molecular orbitals:       29


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a RHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)


  1) Generate initial SAD density


  2) Calculation of reference state (SCF-DIIS algorithm)

  - SCF solver settings:

     Maximum iterations:                   100
     Acceleration type:                   diis

     Residual threshold:            0.1000E-09
     Energy threshold:              0.1000E-09

  - Setting initial AO density to sad

     Energy of initial guess:               -78.492022836338
     Number of electrons in guess:           12.000000000000

  - Screening and integral thresholds:

     Coulomb screening threshold:    0.1000E-15
     Exchange screening threshold:   0.1000E-13
     ERI cutoff:                     0.1000E-15
     One-electron integral  cutoff:  0.1000E-20
     Cumulative Fock threshold:      0.1000E+01

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (solver scf_e): memory
     Storage (solver scf_x): memory

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1           -78.796606592630     0.9053E-01     0.7880E+02
     2           -78.828675852673     0.7128E-01     0.3207E-01
     3           -78.843428154462     0.8290E-02     0.1475E-01
     4           -78.843809541675     0.2414E-02     0.3814E-03
     5           -78.843850846799     0.3301E-03     0.4131E-04
     6           -78.843851669326     0.5527E-04     0.8225E-06
     7           -78.843851692851     0.5865E-05     0.2353E-07
     8           -78.843851693557     0.2356E-05     0.7054E-09
     9           -78.843851693630     0.2071E-06     0.7373E-10
    10           -78.843851693631     0.1594E-07     0.4263E-12
    11           -78.843851693631     0.3322E-08     0.2842E-13
    12           -78.843851693631     0.1197E-08     0.7105E-13
    13           -78.843851693631     0.4264E-09     0.4263E-13
    14           -78.843851693631     0.1280E-09     0.4263E-13
    15           -78.843851693631     0.1479E-10     0.5684E-13
  ---------------------------------------------------------------
  Convergence criterion met in 15 iterations!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.645629080236
     Nuclear repulsion energy:      12.116100574587
     Electronic energy:            -90.959952268218
     Total energy:                 -78.843851693631

  - Timings for the RHF ground state calculation

     Total wall time (sec):              0.36043
     Total cpu time (sec):               0.63225


  :: CCSD wavefunction
  =======================

     Bath orbital(s):         False

   - Number of orbitals:

     Occupied orbitals:    6
     Virtual orbitals:     23
     Molecular orbitals:   29
     Atomic orbitals:      29

   - Number of ground state amplitudes:

     Single excitation amplitudes:  138
     Double excitation amplitudes:  9591


  :: Excited state coupled cluster engine
  ==========================================

  Calculates the coupled cluster excitation vectors and excitation energies

  This is a CCSD excited state calculation.
  The following tasks will be performed:

     1) Cholesky decomposition of the electron repulsion integrals
     2) Preparation of MO basis and integrals
     3) Calculation of the ground state (diis algorithm)
     4) Calculation of the excited state (davidson algorithm)


  1) Cholesky decomposition of the electron repulsion integrals

   - Cholesky decomposition of electronic repulsion integrals solver
  ---------------------------------------------------------------------

  Performs a Cholesky decomposition of the two-electron electronic repulsion 
  integrals in the atomic orbital basis,

  (ab|cd) = sum_J L_ab^J L_cd^J.

  Once the Cholesky basis has been determined, the vectors L^J are constructed 
  and stored to disk. These may either be used directly, or be transformed 
  to the MO basis for use in post-HF calculations. For more information, 
  see S. D. Folkestad, E. F. Kjønstad and H. Koch, JCP, 150(19), (2019)

  - Cholesky decomposition settings:

     Target threshold is:   0.10E-11
     Span factor:           0.10E-01
     Max qual:                  1000

  - Cholesky decomposition ao details:

     Total number of AOs:                    29
     Total number of shell pairs:           120
     Total number of AO pairs:              435

     Significant shell pairs:               118
     Significant AO pairs:                  431

     Construct shell pairs:                 120
     Construct AO pairs:                    435

  Iter.  #Sign. ao pairs / shell pairs   Max diagonal    #Qualified    #Cholesky    Cholesky array size
  -------------------------------------------------------------------------------------------------------
     1               406 /     108       0.47383E+01         147             42             17052
     2               319 /      93       0.47165E-01         234            111             35409
     3               250 /      76       0.46944E-03         178            183             45750
     4               187 /      55       0.38270E-05         145            265             49555
     5                87 /      25       0.38106E-07          78            324             28188
     6                21 /       3       0.37202E-09          43            349              7329
     7                 0 /       0       0.36652E-11           7            354                 0
  -------------------------------------------------------------------------------------------------------

  - Summary of Cholesky decomposition of electronic repulsion integrals:

     Final number of Cholesky vectors: 354

 - Testing the Cholesky decomposition decomposition electronic repulsion integrals:

     Maximal difference between approximate and actual diagonal:              0.7161E-12
     Minimal element of difference between approximate and actual diagonal:  -0.1238E-14

  - Settings for integral handling:

     Cholesky vectors in memory: True
     ERI matrix in memory:       False
     T1 ERI matrix in memory:    True

  - Finished decomposing the ERIs.

     Total wall time (sec):              0.11505
     Total cpu time (sec):               0.22716


  2) Preparation of MO basis and integrals


  3) Calculation of the ground state (diis algorithm)

   - DIIS coupled cluster ground state solver
  ----------------------------------------------

  A DIIS CC ground state amplitude equations solver. It uses an extrapolation 
  of previous quasi-Newton perturbation theory estimates of the next amplitudes. 
  See Helgaker et al., Molecular Electronic Structure Theory, Chapter 
  13.

  - CC ground state solver settings:

     Residual threshold:            0.1000E-09
     Energy threshold:              0.1000E-09
     Max number of iterations:      100

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (cc_gs_diis_e): file
     Storage (cc_gs_diis_x): file

  Iteration    Energy (a.u.)        |omega|       Delta E (a.u.)
  ---------------------------------------------------------------
    1           -79.084241931609     0.9391E-01     0.7908E+02
    2           -79.092586666608     0.2720E-01     0.8345E-02
    3           -79.099408028306     0.7507E-02     0.6821E-02
    4           -79.100345871309     0.2095E-02     0.9378E-03
    5           -79.100371860301     0.5154E-03     0.2599E-04
    6           -79.100393801016     0.2313E-03     0.2194E-04
    7           -79.100385611260     0.4933E-04     0.8190E-05
    8           -79.100384217524     0.1180E-04     0.1394E-05
    9           -79.100383621795     0.4135E-05     0.5957E-06
   10           -79.100383427012     0.1779E-05     0.1948E-06
   11           -79.100383466393     0.6705E-06     0.3938E-07
   12           -79.100383474630     0.2989E-06     0.8237E-08
   13           -79.100383487352     0.1028E-06     0.1272E-07
   14           -79.100383481865     0.3056E-07     0.5487E-08
   15           -79.100383481092     0.6508E-08     0.7730E-09
   16           -79.100383481303     0.2112E-08     0.2109E-09
   17           -79.100383481485     0.6588E-09     0.1819E-09
   18           -79.100383481546     0.1594E-09     0.6158E-10
   19           -79.100383481554     0.5326E-10     0.7915E-11
  ---------------------------------------------------------------
  Convergence criterion met in 19 iterations!

  - Ground state summary:

     Final ground state energy (a.u.):   -79.100383481554

     Correlation energy (a.u.):           -0.256531787923

     Largest single amplitudes:
     -----------------------------------
        a       i         t(a,i)
     -----------------------------------
        1      5        0.014740597486
       14      4       -0.009546856216
        7      4        0.008284826477
       15      5       -0.006124828872
        4      5        0.005606072685
        6      2        0.005476844297
        2      4        0.005318591723
       13      5        0.005269818340
        5      6        0.004933006894
       11      6       -0.003454309400
     ------------------------------------

     Largest double amplitudes:
     --------------------------------------------------
        a      i       b      j         t(ai,bj)
     --------------------------------------------------
        2      4       2      4       -0.047351708919
        5      6       5      6       -0.046240574399
        9      3       9      3       -0.041367012251
        3      4       3      4       -0.036659067518
        6      5       6      5       -0.034554012169
        1      5       1      5       -0.034177347752
       16      3      16      3       -0.032108235347
       17      3      17      3       -0.032052553602
       18      3      18      3       -0.031351828683
        2      4       3      4       -0.029701270697
     --------------------------------------------------

  - Finished solving the CCSD ground state equations

     Total wall time (sec):              0.14870
     Total cpu time (sec):               0.29946


  4) Calculation of the excited state (davidson algorithm)
     Calculating right vectors

   - Davidson coupled cluster excited state solver
  ---------------------------------------------------

  A Davidson solver that calculates the lowest eigenvalues and the right 
  or left eigenvectors of the Jacobian matrix, A. The eigenvalue problem 
  is solved in a reduced space, the dimension of which is expanded until 
  the convergence criteria are met.

  A complete description of the algorithm can be found in E. R. Davidson, 
  J. Comput. Phys. 17, 87 (1975).

  - Settings for coupled cluster excited state solver (Davidson):

     Calculation type:    core
     Excitation vectors:  right

     Residual threshold:            0.1000E-09
     Energy threshold:              0.1000E-09

     Number of singlet states:               2
     Max number of iterations:             100

   - Davidson tool settings:

     Number of parameters:                 9729
     Number of requested solutions:           2
     Max reduced space dimension:           100

     Storage (cc_es_davidson_trials): file
     Storage (cc_es_davidson_transforms): file

  Iteration:                  1
  Reduced space dimension:    2

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1  20.322967277642    0.000000000000     0.9378E+00   0.2032E+02
     2  20.385487902800    0.000000000000     0.9430E+00   0.2039E+02
  ------------------------------------------------------------------------

  Iteration:                  2
  Reduced space dimension:    4

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1  19.764074578665    0.000000000000     0.2436E+00   0.5589E+00
     2  19.830629060416    0.000000000000     0.2672E+00   0.5549E+00
  ------------------------------------------------------------------------

  Iteration:                  3
  Reduced space dimension:    6

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1  19.736463436323    0.000000000000     0.9467E-01   0.2761E-01
     2  19.794046815545    0.000000000000     0.1119E+00   0.3658E-01
  ------------------------------------------------------------------------

  Iteration:                  4
  Reduced space dimension:    8

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1  19.740054603616    0.000000000000     0.6037E-01   0.3591E-02
     2  19.795990268629    0.000000000000     0.7417E-01   0.1943E-02
  ------------------------------------------------------------------------

  Iteration:                  5
  Reduced space dimension:   10

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1  19.743571773035    0.000000000000     0.2525E-01   0.3517E-02
     2  19.800170978436    0.000000000000     0.3308E-01   0.4181E-02
  ------------------------------------------------------------------------

  Iteration:                  6
  Reduced space dimension:   12

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1  19.742565858359    0.000000000000     0.1064E-01   0.1006E-02
     2  19.799062714200    0.000000000000     0.1531E-01   0.1108E-02
  ------------------------------------------------------------------------

  Iteration:                  7
  Reduced space dimension:   14

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1  19.742014047825    0.000000000000     0.2463E-02   0.5518E-03
     2  19.798257172115    0.000000000000     0.3782E-02   0.8055E-03
  ------------------------------------------------------------------------

  Iteration:                  8
  Reduced space dimension:   16

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1  19.741964730648    0.000000000000     0.8987E-03   0.4932E-04
     2  19.798226459618    0.000000000000     0.1359E-02   0.3071E-04
  ------------------------------------------------------------------------

  Iteration:                  9
  Reduced space dimension:   18

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1  19.741967731942    0.000000000000     0.2703E-03   0.3001E-05
     2  19.798254953597    0.000000000000     0.4044E-03   0.2849E-04
  ------------------------------------------------------------------------

  Iteration:                 10
  Reduced space dimension:   20

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1  19.741970130809    0.000000000000     0.1004E-03   0.2399E-05
     2  19.798265429936    0.000000000000     0.1208E-03   0.1048E-04
  ------------------------------------------------------------------------

  Iteration:                 11
  Reduced space dimension:   22

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1  19.741969186323    0.000000000000     0.3217E-04   0.9445E-06
     2  19.798262656807    0.000000000000     0.4043E-04   0.2773E-05
  ------------------------------------------------------------------------

  Iteration:                 12
  Reduced space dimension:   24

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1  19.741968954304    0.000000000000     0.1161E-04   0.2320E-06
     2  19.798262336784    0.000000000000     0.1009E-04   0.3200E-06
  ------------------------------------------------------------------------

  Iteration:                 13
  Reduced space dimension:   26

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1  19.741969049653    0.000000000000     0.3956E-05   0.9535E-07
     2  19.798262278973    0.000000000000     0.2611E-05   0.5781E-07
  ------------------------------------------------------------------------

  Iteration:                 14
  Reduced space dimension:   28

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1  19.741969067271    0.000000000000     0.1041E-05   0.1762E-07
     2  19.798262271445    0.000000000000     0.7188E-06   0.7528E-08
  ------------------------------------------------------------------------

  Iteration:                 15
  Reduced space dimension:   30

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1  19.741969085151    0.000000000000     0.2566E-06   0.1788E-07
     2  19.798262297814    0.000000000000     0.1836E-06   0.2637E-07
  ------------------------------------------------------------------------

  Iteration:                 16
  Reduced space dimension:   32

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1  19.741969094996    0.000000000000     0.7472E-07   0.9845E-08
     2  19.798262306370    0.000000000000     0.4855E-07   0.8556E-08
  ------------------------------------------------------------------------

  Iteration:                 17
  Reduced space dimension:   34

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1  19.741969098270    0.000000000000     0.2510E-07   0.3274E-08
     2  19.798262308267    0.000000000000     0.1372E-07   0.1898E-08
  ------------------------------------------------------------------------

  Iteration:                 18
  Reduced space dimension:   36

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1  19.741969098265    0.000000000000     0.7111E-08   0.4878E-11
     2  19.798262308551    0.000000000000     0.4432E-08   0.2834E-09
  ------------------------------------------------------------------------

  Iteration:                 19
  Reduced space dimension:   38

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1  19.741969098050    0.000000000000     0.1879E-08   0.2158E-09
     2  19.798262308492    0.000000000000     0.1528E-08   0.5902E-10
  ------------------------------------------------------------------------

  Iteration:                 20
  Reduced space dimension:   40

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1  19.741969098021    0.000000000000     0.5523E-09   0.2899E-10
     2  19.798262308490    0.000000000000     0.4824E-09   0.1720E-11
  ------------------------------------------------------------------------

  Iteration:                 21
  Reduced space dimension:   42

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1  19.741969098024    0.000000000000     0.1852E-09   0.3510E-11
     2  19.798262308498    0.000000000000     0.1654E-09   0.8178E-11
  ------------------------------------------------------------------------

  Iteration:                 22
  Reduced space dimension:   44

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1  19.741969098025    0.000000000000     0.6353E-10   0.1005E-11
     2  19.798262308499    0.000000000000     0.4507E-10   0.6644E-12
  ------------------------------------------------------------------------
  Convergence criterion met in 22 iterations!

  - Excitation vector amplitudes:

     Electronic state nr. 1

     Energy (Hartree):                 19.741969098025
     Fraction singles (|R1|/|R|):       0.937559649611

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        1      1       -0.911752873504
        4      1       -0.191488504441
        6      1        0.082472040133
       13      1        0.050868965008
       15      1       -0.035733154383
        9      1        0.012581768436
       10      1       -0.011553648884
       11      1        0.007852331405
       22      1       -0.004153994586
        8      1        0.003990074184
     ------------------------------------

     Largest double amplitudes:
     --------------------------------------------------
        a      i       b      j         R(ai,bj)
     --------------------------------------------------
        1      1       5      6        0.133031485385
        1      1       2      4        0.128205084024
        1      1       6      5       -0.121901866531
        1      1       7      4       -0.108565147983
        1      1       1      5        0.096038707122
        1      1       3      4        0.082134627198
        1      1       1      2        0.080273027762
        1      1       4      5        0.071158693577
        1      1      13      2       -0.063624701856
        1      1      14      4        0.058879152105
     --------------------------------------------------

     Electronic state nr. 2

     Energy (Hartree):                 19.798262308499
     Fraction singles (|R1|/|R|):       0.938869734034

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        2      1       -0.894726115995
        3      1       -0.240526198902
        7      1        0.141166323718
       14      1       -0.054612641261
       12      1        0.011623954955
       23      1       -0.006218983161
        4      1       -0.001403884598
        5      1       -0.001044786697
       17      1        0.000840489247
        1      1        0.000663563181
     ------------------------------------

     Largest double amplitudes:
     --------------------------------------------------
        a      i       b      j         R(ai,bj)
     --------------------------------------------------
        2      1       2      4        0.140615578424
        2      1       5      6        0.128572395886
        2      1       6      5       -0.115185147006
        2      1       7      4       -0.106245116633
        2      1       1      5        0.087769161455
        2      1       3      4        0.079809753606
        2      1       4      5        0.070753011743
        2      1       1      2        0.066609206709
        2      1      13      2       -0.061143586007
        2      1      14      4        0.052163302818
     --------------------------------------------------

     - Electronic excitation energies:

                                      Excitation energy
                          ------------------------------------------
      State                (Hartree)             (eV)
     ---------------------------------------------------------------
        1                 19.741969098025      537.206341921275
        2                 19.798262308499      538.738158201781
     ---------------------------------------------------------------
     eV/Hartree (CODATA 2014): 27.21138602

  - Finished solving the CCSD excited state equations (right)

     Total wall time (sec):              0.21661
     Total cpu time (sec):               0.43232

  - Timings for the CCSD excited state calculation

     Total wall time (sec):              0.48077
     Total cpu time (sec):               0.95936

  ------------------------------------------------------------

  Peak memory usage during the execution of eT: 16.353888 MB

  Total wall time in eT (sec):              0.86245
  Total cpu time in eT (sec):               1.62116

  Calculation ended: 2021-06-07 12:50:25 UTC +02:00

  - Implementation references:

     eT: https://doi.org/10.1063/5.0004713
     Cholesky decomposition of ERIs: https://doi.org/10.1063/1.5083802

  eT terminated successfully!
